import {SnakeNamingStrategy} from "typeorm-naming-strategies";
require('reflect-metadata'); // The "reflect-metadata" is a requirement of typeorm
import dotenv from 'dotenv';
import {createConnection} from "typeorm";
import {error} from "./infrustructure/service/logs/logs";

async function setupEnvironment(): Promise<void> {
    dotenv.config({path: '.env'});
}

async function server(): Promise<void> {
    await setupEnvironment();
    console.log(`[✔] Environment setup`);

    // @ts-ignore
    await createConnection({
        type: "postgres",
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        synchronize: true,
        entities: [
            "dist/domain/entity/**/*.js"
        ],
        namingStrategy: new SnakeNamingStrategy()
    });

    console.log(`[✔] Database setup`);

    const app = require('./app').default;

    /**
     * Start express server.
     */
    app.listen(app.get('port'), () => {
        console.log(
            'App is running at http://localhost:%d in %s mode',
            app.get('port'),
            app.get('env'),
        );
    });
}

server().catch((e: Error) => {
    console.error(`[✕] APP ERROR: `, e);

    error(e.message, e);
});
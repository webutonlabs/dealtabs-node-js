import createMultipleAction from './createMultiple';
import deleteAction from './delete';
import editAction from './edit';
import orderTabsAction from './orderTabs';
import replaceTabsAction from './replaceTabs';
import {getConnection} from "typeorm";
import NotFound from "../../response/exception/NotFound";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {NextFunction, Response} from "express";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {checkCollaboratorSessionAccess} from "../../../infrustructure/security/checkCollaboratorSessionAccess";
import {error} from "../../../infrustructure/service/logs/logs";

export namespace ParamHandler {
    export const id = async (req: ResolvedRequest, res: Response, next: NextFunction, value: any) => {
        try {
            const sessionWindow = await getConnection()
                .getRepository(SessionWindow)
                .findOne(value, {
                    relations: ['session.user', 'session', 'tabs', 'session.windows']
                });

            if (undefined === sessionWindow) {
                return next(new NotFound('Session window was not found'));
            }

            if (sessionWindow.session.user.id !== req.user.id) {
                try {
                    await checkCollaboratorSessionAccess(req.user.email, sessionWindow.session.id);
                } catch (e) {
                    return next(e);
                }
            }

            sessionWindow.tabs = sessionWindow.tabs.sort((a: WindowTab, b: WindowTab) => {
                return a.orderIndex - b.orderIndex;
            });

            req.resolvedParams.sessionWindow = sessionWindow;

            return next();
        } catch (e) {
            error(e.message);

            return next(e);
        }
    };
}

export {
    createMultipleAction,
    deleteAction,
    editAction,
    orderTabsAction,
    replaceTabsAction
}

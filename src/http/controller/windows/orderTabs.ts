import {RequestHandler, Response} from "express";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {filterByIsEnabledStatus} from "../../../infrustructure/helpers";
import {getConnection} from "typeorm";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import SessionWindowNormalizer from "../../response/normalizer/SessionWindowNormalizer";
import {ResolvedRequest} from "../../request/ResolvedRequest";

const validation: RequestHandler[] = [
    body('tabs.*').isInt().withMessage('Tab id must be integer')
]

const orderTabsAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        let sessionWindow: SessionWindow = req.resolvedParams.sessionWindow;
        let tabs: WindowTab[] = sessionWindow.tabs;

        let [enabledTabs, disabledTabs] = filterByIsEnabledStatus(tabs);

        req.body.tabs.forEach((tabId: any, index: number) => {
            for (let i = 0; i < enabledTabs.length; i++) {
                if (enabledTabs[i].id === Number(tabId)) {
                    enabledTabs[i].orderIndex = index;
                }
            }
        });

        let lastOrderedTabIndex = 0;
        enabledTabs.forEach(tab => {
            if (tab.orderIndex > lastOrderedTabIndex) {
                lastOrderedTabIndex = tab.orderIndex;
            }
        })

        for (const disabledTab of disabledTabs) {
            disabledTab.orderIndex = ++lastOrderedTabIndex;
        }

        tabs = [...enabledTabs, ...disabledTabs];

        await getConnection().getRepository(WindowTab).save(tabs);
        sessionWindow.tabs = tabs;

        res.status(200).json(SessionWindowNormalizer.normalize(sessionWindow));
    }
}

const _export: RequestHandler[] = [
    ...validation,
    validationHandler(),
    orderTabsAction()
];

export default _export;
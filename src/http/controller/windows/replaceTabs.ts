import {RequestHandler, Response} from "express";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {getConnection} from "typeorm";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {WindowTabNormalizer} from "../../response/normalizer/WindowTabNormalizer";

interface TabData {
    name: string,
    icon?: string,
    url: string
}

const sanitization: RequestHandler[] = [
    body('tabs.*.name').trim(),
    body('tabs.*.url').trim(),
    body('tabs.*.icon').optional().trim()
]

const validation: RequestHandler[] = [
    body('tabs.*.icon').optional(),
    body('tabs.*.name').notEmpty().withMessage('Tab name cannot be empty'),
    body('tabs.*.url').notEmpty().withMessage('Tab url cannot be empty')
];

const replaceTabsAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        const sessionWindow: SessionWindow = req.resolvedParams.sessionWindow;

        let newTabs: WindowTab[] = [];
        req.body.tabs.forEach((tabData: TabData, tabIndex: number) => {
            let tab = new WindowTab(tabData.name, tabData.url, sessionWindow, tabData.icon);
            tab.orderIndex = tabIndex;

            if (typeof membershipRestrictions.tabsLimit() === 'number' && tabIndex >= membershipRestrictions.tabsLimit()) {
                tab.isEnabled = false;
            }

            newTabs.push(tab);
        });

        await getConnection().transaction(async em => {
            await em.getRepository(WindowTab).remove(sessionWindow.tabs);
            await em.getRepository(WindowTab).save(newTabs);
        });

        sessionWindow.tabs = newTabs;

        res.status(200).json(WindowTabNormalizer.normalizeMany(sessionWindow.tabs));
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    replaceTabsAction()
];

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {body, matchedData} from "express-validator";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {getConnection, getCustomRepository} from "typeorm";
import SessionWindowNormalizer from "../../response/normalizer/SessionWindowNormalizer";
import {validationHandler} from "../../request/validation/validationHandler";
import NotFound from "../../response/exception/NotFound";
import AuthError from "../../response/exception/AuthError";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import BadRequest from "../../response/exception/BadRequest";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('session').optional().trim()
];

const validation: RequestHandler[] = [
    body('name').optional(),
    body('session').optional()
];

const editAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const sessionWindow: SessionWindow = req.resolvedParams.sessionWindow;
        const data: Partial<SessionWindow> = matchedData(req, {locations: ['body']});

        try {
            let oldSessionWindows: SessionWindow[] = [];

            if (data.hasOwnProperty('session')) {
                const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
                const newSession = await getCustomRepository(SessionRepository).findById(Number(data.session));

                if (undefined === newSession) {
                    throw new NotFound('Session was not found');
                }

                if (newSession.user.id !== req.user.id) {
                    throw new AuthError('Access denied.')
                }

                if (newSession.id === sessionWindow.session.id) {
                    throw new BadRequest('Session window is already in given session');
                }

                // When we move a session window to another session
                // we have to make indexes of other windows of the previous session lower
                if (sessionWindow.session.windows.length > 0) {
                    // which are after current session window
                    const windowIndex = sessionWindow.session.windows.findIndex(window => window.id === sessionWindow.id);
                    oldSessionWindows = sessionWindow.session.windows.slice(windowIndex + 1);

                    for (const window of oldSessionWindows) {
                        window.orderIndex--;
                    }
                }

                // if there are no windows in the new session
                let orderIndex = 0;
                if (newSession.windows.length > 0) {
                    // otherwise adding the session window to the end
                    // (windows are ordered by orderIndex in SessionRepository)
                    orderIndex = newSession.windows[newSession.windows.length - 1].orderIndex + 1;
                }

                data.isEnabled = !(typeof membershipRestrictions.windowsLimit() === 'number' && newSession.windows.length >= membershipRestrictions.windowsLimit());
                data.orderIndex = orderIndex;
                data.session = newSession;
            }

            Object.assign(sessionWindow, data);
            await getConnection().getRepository(SessionWindow).save([...oldSessionWindows, sessionWindow]);

            res.status(200).json(SessionWindowNormalizer.normalize(sessionWindow));
        } catch (err) {
            error(err.message, {user_id: req.user.id, stack: err.stack})

            return next(err);
        }
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    editAction()
]

export default _export;
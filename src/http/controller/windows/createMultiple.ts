import {NextFunction, RequestHandler, Response} from "express";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {getConnection, getCustomRepository} from "typeorm";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import SessionWindowNormalizer from "../../response/normalizer/SessionWindowNormalizer";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import NotFound from "../../response/exception/NotFound";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {checkCollaboratorSessionAccess} from "../../../infrustructure/security/checkCollaboratorSessionAccess";

interface TabData {
    name: string,
    icon?: string,
    url: string
}

const sanitization: RequestHandler[] = [
    body('windows.*.tabs.*.name').trim(),
    body('windows.*.tabs.*.url').trim(),
    body('windows.*.tabs.*.icon').trim()
]

const validation: RequestHandler[] = [
    body('windows.*.name').optional(),
    body('windows.*.tabs.*.name').notEmpty().withMessage('Tab name cannot be empty'),
    body('windows.*.tabs.*.url').notEmpty().withMessage('Tab url cannot be empty')
];

const createMultipleAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);

        const session = await getCustomRepository(SessionRepository).findById(req.body.session);
        if (undefined === session) {
            return next(new NotFound('Session was not found'));
        }

        if (req.user.id !== session.user.id) {
            try {
                await checkCollaboratorSessionAccess(req.user.email, session.id);
            } catch (e) {
                return next(e);
            }
        }

        const oldWindows = session.windows;

        let lastOldWindowIndex = -1;
        if (oldWindows.length > 0) {
            lastOldWindowIndex = oldWindows[oldWindows.length - 1].orderIndex;
        }

        let windowsCounter = oldWindows.length;
        let windows = [];
        for (const windowData of req.body.windows) {
            let window = new SessionWindow(session, windowData.name);
            window.orderIndex = ++lastOldWindowIndex;
            window.tabs = [];

            if (typeof membershipRestrictions.windowsLimit() === 'number' && windowsCounter >= membershipRestrictions.windowsLimit()) {
                window.isEnabled = false;
            }

            windowData.tabs.forEach((tabData: TabData, tabIndex: number) => {
                let tab = new WindowTab(tabData.name, tabData.url, window, tabData.icon);
                tab.orderIndex = tabIndex;

                if (typeof membershipRestrictions.tabsLimit() === 'number' && tabIndex >= membershipRestrictions.tabsLimit()) {
                    tab.isEnabled = false;
                }

                window.tabs.push(tab);
            })

            windowsCounter++;
            windows.push(window);
        }

        await getConnection().getRepository(SessionWindow).save(windows);

        res.status(200).json(SessionWindowNormalizer.normalizeMany(windows));
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    createMultipleAction()
]

export default _export;
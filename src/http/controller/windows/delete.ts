import {RequestHandler, Response} from "express";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {getConnection, getCustomRepository} from "typeorm";
import {SessionWindowRepository} from "../../../domain/repository/SessionWindowRepository";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {archiveTabsForUser} from "../../../infrustructure/service/archiveTabs/archiveTabs";

const deleteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        const sessionWindow: SessionWindow = req.resolvedParams.sessionWindow;
        const oldWindows = await getCustomRepository(SessionWindowRepository)
            .findBySessionAndOrderIndex(sessionWindow.session.id, sessionWindow.orderIndex);

        // making old session windows' order index lower
        for (const oldWindow of oldWindows) {
            oldWindow.orderIndex--;

            if (typeof membershipRestrictions.windowsLimit() === 'number' && oldWindow.orderIndex < membershipRestrictions.windowsLimit()) {
                oldWindow.isEnabled = true;
            }
        }

        await archiveTabsForUser(sessionWindow.tabs, req.user);
        await getConnection().transaction(async em => {
            await em.getRepository(SessionWindow).remove(sessionWindow);
            await em.getRepository(SessionWindow).save(oldWindows);
        });

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    deleteAction()
]

export default _export;
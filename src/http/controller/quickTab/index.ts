import createMultiple from "./createMultiple";
import deleteAction from "./delete";
import editAction from "./edit";
import sortAction from "./sort";
import listAction from "./list";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {NextFunction, Response} from "express";
import {getConnection} from "typeorm";
import NotFound from "../../response/exception/NotFound";
import AuthError from "../../response/exception/AuthError";
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";

export namespace ParamHandler {
    export const id = async (req: ResolvedRequest, res: Response, next: NextFunction, value: any) => {
        try {
            const quickTab = await getConnection()
                .getRepository(QuickTab)
                .findOne(value, {
                    relations: ['user']
                });

            if (undefined === quickTab) {
                return next(new NotFound('Quick tab was not found'));
            }

            if (quickTab.user.id !== req.user.id) {
                return next(new AuthError('Access denied.'));
            }

            req.resolvedParams.quickTab = quickTab;

            return next();
        } catch (e) {
            return next(e);
        }
    };
}

export {
    createMultiple,
    deleteAction,
    editAction,
    sortAction,
    listAction
}
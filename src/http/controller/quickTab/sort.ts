import {NextFunction, RequestHandler, Response} from "express";
import {getConnection} from "typeorm";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import AuthError from "../../response/exception/AuthError";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";

const validation: RequestHandler[] = [
    body('tabs.*').isInt().withMessage('Quick tab id must be integer')
]

const orderAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        let quickTabs = await getConnection().getRepository(QuickTab).findByIds(req.body.tabs, {relations: ['user']});

        let indexes = [];
        for (const quickTab of quickTabs) {
            if (quickTab.user.id !== req.user.id) {
                return next(new AuthError('Access denied.'));
            }

            indexes.push(quickTab.orderIndex);
        }

        indexes.sort();

        let newOrder = {};
        for (let i = 0; i < quickTabs.length; i++) {
            // @ts-ignore
            newOrder[req.body.tabs[i]] = indexes[i];
        }

        for (const quickTab of quickTabs) {
            for (const [quickTabId, newIndex] of Object.entries(newOrder)) {
                if (quickTab.id === Number(quickTabId)) {
                    quickTab.orderIndex = Number(newIndex);
                }
            }
        }

        await getConnection().getRepository(QuickTab).save(quickTabs);
        res.status(204).json();
    }
}

const _export = [
    ...validation,
    validationHandler(),
    orderAction()
]

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {body, matchedData} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {getConnection} from "typeorm";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";
import {QuickTabNormalizer} from "../../response/normalizer/QuickTabNormalizer";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('name').optional().trim(),
    body('url').optional().trim(),
    body('icon').optional().trim()
];

const validation: RequestHandler[] = [
    body('name').optional().notEmpty().withMessage('Name cannot be empty'),
    body('url').optional().notEmpty().withMessage('Name cannot be empty'),
    body('icon').optional()
];

const editAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const quickTab = req.resolvedParams.quickTab;
        const data: Partial<QuickTab> = matchedData(req, {locations: ['body']});

        try {
            Object.assign(quickTab, data);
            await getConnection().getRepository(QuickTab).save([quickTab]);

            res.status(200).json(QuickTabNormalizer.normalize(quickTab));
        } catch (err) {
            return next(err);
        }
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    editAction()
];

export default _export;
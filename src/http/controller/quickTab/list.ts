import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {PaginationResolver} from "../../response/pagination/PaginationResolver";
import {QuickTabRepository} from "../../../domain/repository/QuickTabRepository";
import {QuickTabNormalizer} from "../../response/normalizer/QuickTabNormalizer";

const listAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const pagination = await getConnection()
            .getCustomRepository(QuickTabRepository)
            .listForUser(req.user);

        new PaginationResolver(pagination, res)
            .setHeaders().status(200).json(QuickTabNormalizer.normalizeMany(pagination.data));
    }
}

const _export = [
    listAction()
]

export default _export;
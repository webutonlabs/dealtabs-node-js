import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {getConnection} from "typeorm";
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";
import {QuickTabNormalizer} from "../../response/normalizer/QuickTabNormalizer";

const sanitization: RequestHandler[] = [
    body('tabs.*.name').trim(),
    body('tabs.*.url').trim(),
    body('tabs.*.icon').optional().trim()
]

const validation: RequestHandler[] = [
    body('tabs.*.name').notEmpty().withMessage('Tab name cannot be empty'),
    body('tabs.*.url').notEmpty().withMessage('Tab url cannot be empty'),
    body('tabs.*.icon').optional()
];

const createMultipleAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        const quickTabs:QuickTab[] = await getConnection().getRepository(QuickTab).find({
            where: {
                user: req.user
            },
            order: {
                orderIndex: "DESC"
            }
        });

        // getting last index
        let orderIndex = -1;
        if (quickTabs.length > 0) {
            orderIndex = quickTabs[0].orderIndex
        }

        let tabsCounter = quickTabs.length;
        let tabs = [];

        for (const tabData of req.body.tabs) {
            let tab = new QuickTab(req.user, tabData.name, tabData.url, tabData.icon);
            tab.orderIndex = ++orderIndex;

            if (typeof membershipRestrictions.quickTabsLimit() === 'number' && tabsCounter >= membershipRestrictions.tabsLimit()) {
                tab.isEnabled = false;
            }

            tabs.push(tab);
            tabsCounter++;
        }

        await getConnection().getRepository(QuickTab).save(tabs);
        res.status(200).json(QuickTabNormalizer.normalizeMany(tabs));
    }
}

const _export : RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    createMultipleAction()
];

export default _export;
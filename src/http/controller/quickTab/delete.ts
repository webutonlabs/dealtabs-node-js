import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection, getCustomRepository} from "typeorm";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {archiveTabsForUser} from "../../../infrustructure/service/archiveTabs/archiveTabs";
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";
import {QuickTabRepository} from "../../../domain/repository/QuickTabRepository";

const deleteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        const quickTab: QuickTab = req.resolvedParams.quickTab;
        const oldTabs: QuickTab[] = await getCustomRepository(QuickTabRepository)
            .findByUserAndOrderIndex(req.user.id, quickTab.orderIndex);

        for (const oldTab of oldTabs) {
            oldTab.orderIndex--;

            // enabling old tab in case if it is on the border of tabs limit
            if (typeof membershipRestrictions.quickTabsLimit() === 'number' && oldTab.orderIndex <= membershipRestrictions.quickTabsLimit()) {
                oldTab.isEnabled = true;
            }
        }

        await archiveTabsForUser([quickTab], req.user);
        await getConnection().transaction(async em => {
            await em.getRepository(QuickTab).remove(quickTab);
            await em.getRepository(QuickTab).save(oldTabs);
        });

        res.status(204).json();
    }
}

const _export : RequestHandler[] = [
    deleteAction()
];

export default _export;
import makeUserPremium from './makeUserPremium';
import makeUserBasic from './makeUserBasic';
import cancelSubscription from './cancelSubscription';

export {
    makeUserPremium,
    makeUserBasic,
    cancelSubscription
}
import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {body, matchedData} from "express-validator";
import {getConnection} from "typeorm";
import {User} from "../../../domain/entity/user/User";
import {validationHandler} from "../../request/validation/validationHandler";
import {Membership, MembershipTypes, SubscriptionTypes} from "../../../domain/entity/user/Membership";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {Session} from "../../../domain/entity/session/Session";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {failIfUserNotExistByEmail} from "../../request/validation/customValidators/failIfUserNotExistByEmail";
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {translator} from "../../../infrustructure/service/translation/translator";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('user').trim()
];

const validation: RequestHandler[] = [
    body('user').isEmail().custom(failIfUserNotExistByEmail)
];

const makeUserBasicAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const data = matchedData(req, {locations: ['body']});
        const user: User = await getConnection().getRepository(User).findOne({
            where: {
                email: data.user
            },
            relations: ['membership', 'sessions', 'sessions.windows', 'sessions.windows.tabs', 'quickTabs']
        });

        const membership: Membership = user.membership;
        membership.cancelledSubscription = false;
        membership.subscriptionType = SubscriptionTypes.none;
        membership.membershipType = MembershipTypes.basic;
        membership.subscriptionExpires = null;

        const membershipRestrictions = new MembershipRestrictions().forBasic();
        user.sessions.forEach((session: Session) => {
            session.enabledForCollaboration = false;

            if (session.orderIndex >= membershipRestrictions.sessionLimit()) {
                session.isEnabled = false;
            }

            session.windows.forEach((window: SessionWindow) => {
                if (window.orderIndex >= membershipRestrictions.windowsLimit()) {
                    window.isEnabled = false;
                }

                window.tabs.forEach((tab: WindowTab) => {
                    if (tab.orderIndex >= membershipRestrictions.tabsLimit()) {
                        tab.isEnabled = false;
                    }
                })
            })
        });

        user.quickTabs.forEach((quickTab: QuickTab) => {
            if (typeof membershipRestrictions.quickTabsLimit() === 'number' && quickTab.orderIndex >= membershipRestrictions.quickTabsLimit()) {
                quickTab.isEnabled = false;
            }
        });

        const collaboratedSessions = await getConnection()
            .getCustomRepository(CollaboratedSessionRepository).listByOwner(user);
        for (const session of collaboratedSessions) {
            session.isActive = false;
        }

        await getConnection().transaction(async em => {
            await em.getRepository(Membership).save(membership);
            await em.getRepository(Session).save(user.sessions);
            await em.getRepository(QuickTab).save(user.quickTabs);
            await em.getRepository(CollaboratedSession).save(collaboratedSessions);
        });

        notificationsClient().post('/notifications', {
            type: 'text',
            context: {
                user: {
                    id: user.id
                },
                content: translator(user.locale).trans('switched-to-basic')
            }
        }).then(() => {
            //
        }).catch(err => {
            error(err.message, {user_id: user.id, stack: err.stack})
        });

        notificationsClient().delete(`/notifications?scopes=warning,alert&user_id=${user.id}`)
            .then(() => {
                //
            })
            .catch(err => {
                error(err.message, {user_id: user.id, stack: err.stack})
            });

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    makeUserBasicAction()
];

export default _export;
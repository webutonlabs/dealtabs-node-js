import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {body, matchedData} from "express-validator";
import {getConnection} from "typeorm";
import {User} from "../../../domain/entity/user/User";
import {failIfUserNotExistByEmail} from "../../request/validation/customValidators/failIfUserNotExistByEmail";
import {validationHandler} from "../../request/validation/validationHandler";
import {Membership, MembershipTypes, SubscriptionTypes} from "../../../domain/entity/user/Membership";
import BadRequest from "../../response/exception/BadRequest";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {translator} from "../../../infrustructure/service/translation/translator";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('user').trim()
];

const validation: RequestHandler[] = [
    body('user').isEmail().custom(failIfUserNotExistByEmail)
];

const cancelSubscriptionAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const data = matchedData(req, {locations: ['body']});
        const user: User = await getConnection().getRepository(User).findOne({
            where: {
                email: data.user
            },
            relations: ['membership']
        });

        const membership: Membership = user.membership;
        if (![MembershipTypes.premium].includes(<MembershipTypes.premium> membership.membershipType)) {
            return next(new BadRequest('User has not any subscriptions'));
        }

        membership.cancelledSubscription = true;
        membership.subscriptionType = SubscriptionTypes.expiring;
        await getConnection().getRepository(Membership).save(membership);

        notificationsClient().post('/notifications', {
            type: 'text',
            context: {
                user: {
                    id: user.id
                },
                content: translator(user.locale).trans('subscription-was-cancelled')
            }
        }).then(() => {
            //
        }).catch(err => {
            error(err.message, {user_id: user.id, stack: err.stack})
        });

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    cancelSubscriptionAction()
];

export default _export;
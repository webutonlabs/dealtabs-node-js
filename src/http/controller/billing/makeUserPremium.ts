import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {body, matchedData} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {getConnection} from "typeorm";
import {User} from "../../../domain/entity/user/User";
import {Membership} from "../../../domain/entity/user/Membership";
import {Session} from "../../../domain/entity/session/Session";
import {failIfUserNotExistByEmail} from "../../request/validation/customValidators/failIfUserNotExistByEmail";
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {translator} from "../../../infrustructure/service/translation/translator";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('user').trim(),
    body('subscription_type').trim(),
    body('membership_type').trim(),
    body('subscription_expires').trim().toDate(),
];

const validation: RequestHandler[] = [
    body('user').isEmail().custom(failIfUserNotExistByEmail),
    body('subscription_type').matches(/\b(?:gift|liqpay_one_time_month|liqpay_one_time_year|liqpay_recurring_monthly|liqpay_recurring_yearly|fondy_one_time_month|fondy_one_time_year|fondy_recurring_monthly|fondy_recurring_yearly)\b/),
    body('membership_type').matches(/\b(?:premium)\b/),
    body('subscription_expires').notEmpty(),
    body('allowed_trial').optional().isBoolean()
];

const makeUserPremiumAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const data = matchedData(req, {locations: ['body']});
        const user: User = await getConnection().getRepository(User).findOne({
            where: {
                email: data.user
            },
            relations: ['membership', 'sessions', 'sessions.windows', 'sessions.windows.tabs', 'quickTabs']
        });

        const membership: Membership = user.membership;
        membership.cancelledSubscription = false;
        membership.subscriptionType = data['subscription_type'];
        membership.membershipType = data['membership_type'];
        membership.subscriptionExpires = new Date(data['subscription_expires']);

        if (data.hasOwnProperty('allowed_trial')) {
            membership.allowedTrial = data['allowed_trial'];
        }

        // Enabling all sessions, windows and tabs due to subscription plan conditions
        for (const session of user.sessions) {
            session.isEnabled = true;

            for (const window of session.windows) {
                window.isEnabled = true;

                for (const tab of window.tabs) {
                    tab.isEnabled = true;
                }
            }
        }

        for (const tab of user.quickTabs) {
            tab.isEnabled = true;
        }

        const collaboratedSessions = await getConnection()
            .getCustomRepository(CollaboratedSessionRepository).listByOwner(user);
        for (const session of collaboratedSessions) {
            session.isActive = true;
        }

        await getConnection().transaction(async em => {
            await em.getRepository(Membership).save(membership);
            await em.getRepository(Session).save(user.sessions);
            await em.getRepository(QuickTab).save(user.quickTabs);
            await em.getRepository(CollaboratedSession).save(collaboratedSessions);
        });

        notificationsClient().post('/notifications', {
            type: 'text',
            context: {
                user: {
                    id: user.id
                },
                content: translator(user.locale).trans('switched-to-premium')
            }
        }).then(() => {
            //
        }).catch(err => {
            error(err.message, {user_id: user.id, stack: err.stack})
        });

        notificationsClient().delete(`/notifications?scopes=warning,alert&user_id=${user.id}`)
            .then(() => {
                //
            })
            .catch(err => {
                error(err.message, {user_id: user.id, stack: err.stack})
            });

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    makeUserPremiumAction()
];

export default _export;
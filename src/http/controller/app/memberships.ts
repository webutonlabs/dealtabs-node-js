import {RequestHandler, Response} from "express";
import {membershipTypesCollection} from "../../../config/membership";
import {default as snake} from "snakecase-keys";
import {ResolvedRequest} from "../../request/ResolvedRequest";

const viewAction = (): RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        res.status(200).json(snake(membershipTypesCollection));
    }
}

const _export: RequestHandler[] = [
    viewAction()
]

export default _export;
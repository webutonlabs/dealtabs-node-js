import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";

/**
 * Extensions will be blocked if they are under this version
 */
const viewBlockingVersionAction = (): RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        res.status(200).json({version: parseFloat(process.env.EXTENSIONS_BLOCKING_VERSION)});
    }
}

const _export: RequestHandler[] = [
    viewBlockingVersionAction()
]

export default _export;
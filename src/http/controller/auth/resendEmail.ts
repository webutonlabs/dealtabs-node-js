import {RequestHandler, Response} from "express";
import {body} from "express-validator";
import {getConnection} from "typeorm";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {User} from "../../../domain/entity/user/User";
import {validationHandler} from "../../request/validation/validationHandler";
import {ConfirmEmailToken} from "../../../domain/entity/user/ConfirmEmailToken";
import {failIfUserNotExistByEmail} from "../../request/validation/customValidators/failIfUserNotExistByEmail";
import {notificationsEmailClient} from "../../../infrustructure/externalService/notificationService/notificationsEmailClient";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('email').trim()
]

const validation: RequestHandler[] = [
    body('email').custom(failIfUserNotExistByEmail)
]

const resendEmailAction: RequestHandler = async (req: ResolvedRequest, res: Response) => {
    const user = await getConnection().getRepository(User).findOne({
        where: {
            email: req.body.email
        }
    });

    const confirmEmailToken = await getConnection().getRepository(ConfirmEmailToken).findOne({
        where: {
            user: user
        }
    });

    const emailClient = notificationsEmailClient(user.locale);
    emailClient.post('/confirm-email', {
        user: {
            name: user.name,
            email: user.email
        },
        context: {
            token: confirmEmailToken.token
        }
    }).then((res) => {
        //
    }).catch(err => {
        error(err.message, {user_id: user.id, stack: err.stack});
    })

    res.status(204).json();
}

const _export = [
    ...sanitization,
    ...validation,
    validationHandler(),
    resendEmailAction
]

export default _export;
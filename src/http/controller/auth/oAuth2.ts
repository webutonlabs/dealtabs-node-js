import {NextFunction, RequestHandler, Response} from "express";
import {validationHandler} from "../../request/validation/validationHandler";
import {body, matchedData} from "express-validator";
import AuthError from "../../response/exception/AuthError";
import {getConnection} from "typeorm";
import {User} from "../../../domain/entity/user/User";
import {default as bcrypt} from "bcrypt";
import {AccessToken} from "../../../domain/entity/user/AccessToken";
import {v4 as uuid} from "uuid";
import {AccessTokenNormalizer} from "../../response/normalizer/AccessTokenNormalizer";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {registerFromSocials} from "../../../infrustructure/service/user/userAuth";
import axios from "axios";
import {createAuth} from "../../../infrustructure/externalService/google/googleUtil";
import {google} from "googleapis";

const enum GrantType {
    username = 'username_password',
    facebook = 'facebook_access_token',
    google = 'google_code'
}

interface UsernameCredentials {
    login: string,
    password: string
}

interface FacebookCredentials {
    access_token: string
}

interface GoogleCredentials {
    code: string
}

interface AuthRequest {
    grant_type: GrantType,
    credentials: UsernameCredentials | FacebookCredentials
}

const sanitization: RequestHandler[] = [
    body('credentials.login').optional().trim(),
    body('credentials.password').optional().trim(),
    body('credentials.access_token').optional().trim(),
    body('credentials.code').optional().trim()
]

const validation: RequestHandler[] = [
    body('grant_type').matches(/\b(?:username_password|facebook_access_token|google_code)\b/),
    body('credentials.login').optional().notEmpty().isLength({min: 6}),
    body('credentials.password').optional().notEmpty().isLength({min: 3}),
    body('credentials.access_token').optional().notEmpty(),
    body('credentials.code').optional().notEmpty()
]

const authAction = (): RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const authRequest: Partial<AuthRequest> = matchedData(req, {locations: ['body']});

        if (authRequest.grant_type === GrantType.username) {
            //@ts-ignore
            return authByUsername(authRequest.credentials, req, res, next);
        }

        if (authRequest.grant_type === GrantType.facebook) {
            //@ts-ignore
            return authByFacebook(authRequest.credentials, req, res, next);
        }

        if (authRequest.grant_type === GrantType.google) {
            //@ts-ignore
            return authByGoogle(authRequest.credentials, req, res, next);
        }

        return next(new AuthError());
    }
}

const authByUsername = async (credentials: UsernameCredentials, req: ResolvedRequest, res: Response, next: NextFunction) => {
    const user = await getConnection().getRepository(User).findOne(
        {
            where: {
                email: credentials.login
            }
        }
    )

    if (undefined === user) {
        return next(new AuthError('User was not found'));
    }

    if (!user.isEmailConfirmed) {
        return next(new AuthError('You must confirm your email address!'));
    }

    if (user.password === null || user.password === '') {
        return next(new AuthError('You were registered by socials and didn\'t set your password yet. Please auth using any socials button.'));
    }

    if (!bcrypt.compareSync(credentials.password, user.password)) {
        return next(new AuthError('Password is invalid'));
    }

    await createAndSendAccessToken(res, user);
}

const authByGoogle = async (credentials: GoogleCredentials, req: ResolvedRequest, res: Response, next: NextFunction) => {
    const auth = createAuth();
    const data = await auth.getToken(credentials.code);
    auth.setCredentials(data.tokens);

    const people = google.people({version: "v1", auth});
    const fields = [
        "names",
        "emailAddresses"
    ];

    people.people.get({resourceName: "people/me", personFields: fields.join(',')})
        .then(async userData => {
            const name = userData.data.names[0].givenName;
            const surname = userData.data.names[0].familyName;
            const email = userData.data.emailAddresses[0].value.toLowerCase();

            const user = await getConnection()
                .getRepository(User).findOne({where: {email: email}});

            if (undefined === user) {
                registerFromSocials(name, email, req.body.context.locale, surname).then(async user => {
                    await createAndSendAccessToken(res, user);
                });
            } else {
                await createAndSendAccessToken(res, user);
            }
        });
}

const authByFacebook = async (credentials: FacebookCredentials, req: ResolvedRequest, res: Response, next: NextFunction) => {
    axios({
        url: 'https://graph.facebook.com/me',
        method: 'get',
        params: {
            fields: ['email', 'first_name', 'last_name'].join(','),
            access_token: credentials.access_token,
        },
    }).then(async response => {
        const userData = response.data;
        const user = await getConnection()
            .getRepository(User).findOne({where: {email: userData['email']}});

        if (undefined === user) {
            registerFromSocials(userData['first_name'], userData['email'].toLowerCase(), req.body.context.locale, userData['last_name']).then(async user => {
                await createAndSendAccessToken(res, user);
            });
        } else {
            await createAndSendAccessToken(res, user);
        }
    });
}

const createAndSendAccessToken = async (res: Response, user: User) => {
    const currentDate = new Date();
    const accessToken = new AccessToken(
        uuid(), new Date(currentDate.setMonth(currentDate.getMonth() + 6)), user
    );

    await getConnection().getRepository(AccessToken).save(accessToken);

    res.status(200).json(AccessTokenNormalizer.normalize(accessToken));
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    authAction()
];

export default _export;
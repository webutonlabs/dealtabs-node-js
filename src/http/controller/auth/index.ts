import authAction from './oAuth2';
import registerByUsernameAction from './registerByUsername';
import requestPasswordRecovery from './requestPasswordRecovery';
import changePassword from './changePassword';
import confirmEmail from './confirmEmail';
import resendEmail from './resendEmail';

export {
    authAction,
    registerByUsernameAction,
    requestPasswordRecovery,
    changePassword,
    confirmEmail,
    resendEmail
}
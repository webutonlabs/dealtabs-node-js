import {RequestHandler, Response} from "express";
import {body} from "express-validator";
import {getConnection} from "typeorm";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {User} from "../../../domain/entity/user/User";
import {validationHandler} from "../../request/validation/validationHandler";
import {ConfirmEmailToken} from "../../../domain/entity/user/ConfirmEmailToken";
import {notificationsEmailClient} from "../../../infrustructure/externalService/notificationService/notificationsEmailClient";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('token').trim()
]

const validation: RequestHandler[] = [
    body('token').notEmpty().isLength({min: 23, max: 25}),
]

const confirmEmailAction: RequestHandler = async (req: ResolvedRequest, res: Response) => {
    const confirmEmailToken = await getConnection()
        .getRepository(ConfirmEmailToken).findOne({
            where: {token: req.body.token},
            relations: ['user']
        });

    const user = confirmEmailToken.user;
    user.isEmailConfirmed = true;

    await getConnection().transaction(async em => {
        await em.getRepository(ConfirmEmailToken).remove(confirmEmailToken);
        await em.getRepository(User).save(user);
    });

    const emailClient = notificationsEmailClient(user.locale);
    emailClient.post('/welcome-message', {
        user: {
            name: user.name,
            email: user.email
        }
    }).then((res) => {
        //
    }).catch(err => {
        error(err.message, {user_id: user.id, stack: err.stack});
    })

    res.status(204).json();
}

const _export = [
    ...sanitization,
    ...validation,
    validationHandler(),
    confirmEmailAction
]

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {validationHandler} from "../../request/validation/validationHandler";
import {body, matchedData} from "express-validator";
import {User} from "../../../domain/entity/user/User";
import {getConnection} from "typeorm";
import {Membership, MembershipTypes, SubscriptionTypes} from "../../../domain/entity/user/Membership";
import {default as bcrypt} from "bcrypt";
import {UserNormalizer} from "../../response/normalizer/UserNormalizer";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {failIfUserExistByEmail} from "../../request/validation/customValidators/failIfUserExistByEmail";
import {ConfirmEmailToken} from "../../../domain/entity/user/ConfirmEmailToken";
import {randomCode} from "../../../infrustructure/helpers";
import {notificationsEmailClient} from "../../../infrustructure/externalService/notificationService/notificationsEmailClient";
import {error} from "../../../infrustructure/service/logs/logs";

interface RegistrationData {
    name: string,
    surname?: string,
    password: string,
    email: string,
    locale?: string
}

const sanitization: RequestHandler[] = [
    body('name').trim(),
    body('surname').optional().trim(),
    body('password').trim(),
    body('email').trim(),
    body('locale').optional().trim()
]

const validation: RequestHandler[] = [
    body('name').notEmpty().isLength({min: 3}),
    body('surname').optional().isLength({min: 3}),
    body('password').notEmpty().isLength({min: 6}),
    body('email').notEmpty().isEmail().custom(failIfUserExistByEmail),
    body('locale').notEmpty()
]

const registerByUsernameAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const registrationData: Partial<RegistrationData> = matchedData(req, {locations: ['body']});
        registrationData.email = registrationData.email.toLowerCase();

        const user = new User();
        Object.assign(user, registrationData);
        user.isEmailConfirmed = false;

        const currentDate = new Date();
        user.membership = new Membership(
            MembershipTypes.premium,
            SubscriptionTypes.gift,
            new Date(currentDate.setMonth(currentDate.getMonth() + 6))
        );

        user.password = bcrypt.hashSync(user.password, 10);

        const confirmEmailToken = new ConfirmEmailToken(randomCode(24), user);

        const emailClient = notificationsEmailClient(user.locale);
        emailClient.post('/confirm-email', {
            user: {
                name: user.name,
                email: user.email
            },
            context: {
                token: confirmEmailToken.token
            }
        }).then((res) => {
            //
        }).catch(err => {
            error(err.message, {user_id: user.id, stack: err.stack});
        })

        try {
            await getConnection().transaction(async em => {
                await em.getRepository(User).save(user);
                await em.getRepository(ConfirmEmailToken).save(confirmEmailToken);
            })
        } catch (e) {
            return next(e);
        }

        res.status(200).json(UserNormalizer.normalize(user));
    }
}

const _export = [
    ...sanitization,
    ...validation,
    validationHandler(),
    registerByUsernameAction()
];

export default _export;
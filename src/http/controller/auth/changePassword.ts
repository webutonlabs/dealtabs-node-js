import {RequestHandler, Response} from "express";
import {body} from "express-validator";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {validationHandler} from "../../request/validation/validationHandler";
import {PasswordRecoveryCode} from "../../../domain/entity/user/PasswordRecoveryCode";
import {getConnection} from "typeorm";
import {User} from "../../../domain/entity/user/User";
import {default as bcrypt} from "bcrypt";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {translator} from "../../../infrustructure/service/translation/translator";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('password').trim(),
    body('code').trim()
]

const validation: RequestHandler[] = [
    body('password').notEmpty().isLength({min: 6}),
    body('code').notEmpty().isLength({min: 9}).custom(async code => {
        const passwordRecoveryCode = await getConnection()
            .getRepository(PasswordRecoveryCode).findOne({where: {code: code}});

        if (undefined === passwordRecoveryCode) {
            throw new Error('Code was not found');
        }

        return true;
    })
]

const requestPasswordRecoveryAction: RequestHandler = async (req: ResolvedRequest, res: Response) => {
    const passwordRecoveryCode = await getConnection()
        .getRepository(PasswordRecoveryCode).findOne({
            where: {code: req.body.code},
            relations: ['user']
        });

    const user = passwordRecoveryCode.user;
    user.password = bcrypt.hashSync(req.body.password, 10);

    await getConnection().transaction(async em => {
        await em.getRepository(PasswordRecoveryCode).remove(passwordRecoveryCode);
        await em.getRepository(User).save(user);
    });

    notificationsClient().post('/notifications', {
        type: 'text',
        context: {
            user: {
                id: user.id
            },
            content: translator(user.locale).trans('password-changed')
        }
    }).then(() => {
        //
    }).catch(err => {
        error(err.message, {user_id: user.id, stack: err.stack});
    });

    res.status(204).json();
}

const _export = [
    ...sanitization,
    ...validation,
    validationHandler(),
    requestPasswordRecoveryAction
]

export default _export;
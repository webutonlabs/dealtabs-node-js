import {RequestHandler, Response} from "express";
import {body} from "express-validator";
import {failIfUserNotExistByEmail} from "../../request/validation/customValidators/failIfUserNotExistByEmail";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {validationHandler} from "../../request/validation/validationHandler";
import {PasswordRecoveryCode} from "../../../domain/entity/user/PasswordRecoveryCode";
import {randomCode} from "../../../infrustructure/helpers";
import {getConnection} from "typeorm";
import {User} from "../../../domain/entity/user/User";
import {notificationsEmailClient} from "../../../infrustructure/externalService/notificationService/notificationsEmailClient";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('email').trim().normalizeEmail()
]

const validation: RequestHandler[] = [
    body('email').notEmpty().isEmail().custom(failIfUserNotExistByEmail)
]

const requestPasswordRecoveryAction: RequestHandler = async (req: ResolvedRequest, res: Response) => {
    const user: User = await getConnection().getRepository(User).findOne({where: {email: req.body.email}});
    const passwordRecoveryCode = new PasswordRecoveryCode(randomCode(), user);

    await getConnection().getRepository(PasswordRecoveryCode).save(passwordRecoveryCode);

    const emailClient = notificationsEmailClient(user.locale);
    emailClient.post('/get-password-recovery-code', {
        user: {
            name: user.name,
            email: user.email
        },
        context: {
            code: passwordRecoveryCode.code
        }
    }).then((res) => {
        //
    }).catch(err => {
        error(err.message, {user_id: user.id, stack: err.stack});
    })

    res.status(204).json();
}

const _export = [
    ...sanitization,
    ...validation,
    validationHandler(),
    requestPasswordRecoveryAction
]

export default _export;
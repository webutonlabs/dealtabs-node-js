import {RequestHandler, Response} from "express";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {getConnection} from "typeorm";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {filterByIsEnabledStatus} from "../../../infrustructure/helpers";
import SessionNormalizer from "../../response/normalizer/SessionNormalizer";
import {ResolvedRequest} from "../../request/ResolvedRequest";

const validation: RequestHandler[] = [
    body('windows.*').isInt().withMessage('Window id must be integer')
]

const orderAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        let session = req.resolvedParams.session;
        let windows: SessionWindow[] = session.windows;

        let [enabledWindows, disabledWindows] = filterByIsEnabledStatus(windows);

        req.body.windows.forEach((windowId: any, index: number) => {
            for (let i = 0; i < enabledWindows.length; i++) {
                if (enabledWindows[i].id === Number(windowId)) {
                    enabledWindows[i].orderIndex = index;
                }
            }
        });

        let lastOrderedWindowIndex = 0;
        enabledWindows.forEach(window => {
            if (window.orderIndex > lastOrderedWindowIndex) {
                lastOrderedWindowIndex = window.orderIndex;
            }
        })

        for (const disabledWindow of disabledWindows) {
            disabledWindow.orderIndex = ++lastOrderedWindowIndex;
        }

        windows = [...enabledWindows, ...disabledWindows];

        await getConnection().getRepository(SessionWindow).save(windows);
        session.windows = windows;

        res.status(200).json(SessionNormalizer.normalize(session));
    }
}

const _export = [
    ...validation,
    validationHandler(),
    orderAction()
]

export default _export;
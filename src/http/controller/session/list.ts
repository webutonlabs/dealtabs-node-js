import {RequestHandler, Response} from "express";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {query} from "express-validator";
import {QueryParams} from "../../request/query/QueryParams";
import {getConnection, getCustomRepository} from "typeorm";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import SessionNormalizer from "../../response/normalizer/SessionNormalizer";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {Session} from "../../../domain/entity/session/Session";

const sanitization: RequestHandler[] = [
    query('*').trim()
]

const listAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        const queryParams = new QueryParams(req.query);
        const userSessions = await getCustomRepository(SessionRepository).listUserSessions(
            req.user,
            queryParams.getBoolean('all') || membershipRestrictions.deviceSync(),
            req.deviceId,
            queryParams.getMultiple('relations')
        );

        const collaboratedSessions = await getConnection()
            .getCustomRepository(CollaboratedSessionRepository).listByUser(req.user);

        let sessions = [...userSessions];
        for (const collaboratedSession of collaboratedSessions) {
            let notThisUserSession = collaboratedSession.relatedSession;
            if (notThisUserSession.isEnabled) {
                // filling meta data stored in collaboratedSession entity
                notThisUserSession.orderIndex = collaboratedSession.orderIndex;
                notThisUserSession.owned = false;

                sessions = [notThisUserSession, ...sessions];
            }
        }

        sessions.sort((a: Session, b: Session) => (a.orderIndex > b.orderIndex) ? 1 : -1);
        const [offset, sliceEnd, totalItems, nextPage] = pagination(queryParams, sessions);

        sessions = sessions.slice(offset, sliceEnd);

        res.setHeader('X-PAGINATION-TOTAL', totalItems);
        res.setHeader('X-PAGINATION-NEXT-PAGE', nextPage);
        res.status(200).json(SessionNormalizer.normalizeMany(sortSessionRelations(sessions)));
    }
}

const pagination = (queryParams: QueryParams, sessions: Session[]) => {
    const perPage = Number(queryParams.get('per_page'));
    const page = Number(queryParams.get('page'));
    let offset = 0;

    if (page > 1) {
        offset = perPage * (page - 1);
    }

    const sliceEnd = offset + perPage;
    const totalItems = sessions.length;

    let nextPage = page + 1;
    if (sessions.slice(offset + perPage, sliceEnd + perPage).length === 0) {
        nextPage = null;
    }

    return [offset, sliceEnd, totalItems, nextPage];
}

const sortSessionRelations = (sessions: Session[]): Session[] => {
    for (const session of sessions) {
        if (session.windows !== undefined) {
            session.windows.sort((a: SessionWindow, b: SessionWindow) => (a.orderIndex > b.orderIndex) ? 1 : -1);

            for (const window of session.windows) {
                if (window.tabs !== undefined) {
                    window.tabs.sort((a: WindowTab, b: WindowTab) => (a.orderIndex > b.orderIndex) ? 1 : -1);
                }
            }
        }
    }

    return sessions;
}

const _export = [
    ...sanitization,
    listAction()
]

export default _export;
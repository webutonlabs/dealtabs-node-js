import {ResolvedRequest} from "../../request/ResolvedRequest";
import {RequestHandler, Response} from "express";
import SessionNormalizer from "../../response/normalizer/SessionNormalizer";

const viewAction: RequestHandler = async (req: ResolvedRequest, res: Response) => {
    res.status(200).json(SessionNormalizer.normalize(req.resolvedParams.session));
}

const _export = [
    viewAction
];

export default _export;
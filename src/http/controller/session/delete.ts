import {NextFunction, RequestHandler, Response} from "express";
import {param} from "express-validator";
import {getConnection, getCustomRepository} from "typeorm";
import {Session} from "../../../domain/entity/session/Session";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {isUserPremium} from "../../../infrustructure/helpers";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {archiveTabsForUser} from "../../../infrustructure/service/archiveTabs/archiveTabs";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {decrementIndexesOfOtherSessions} from "../../../infrustructure/service/sorting/orderIndexHelpers";
import AuthError from "../../response/exception/AuthError";

const sanitization: RequestHandler[] = [
    param('id').trim(),
]

const deleteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const session = req.resolvedParams.session;

        if (req.user.id !== session.user.id) { // collaborators cannot remove sessions
            return next(new AuthError('Access denied.'));
        }

        let thisDeviceSessions: Session[] = [];
        let oldSessions = await getCustomRepository(SessionRepository)
            .findByUserAndOrderIndex(req.user.id, session.orderIndex);
        let oldCollaboratedSessions = await getCustomRepository(CollaboratedSessionRepository).listByUser(req.user);

        // making old sessions' order index lower
        for (const oldSession of [...oldSessions, ...oldCollaboratedSessions]) {
            if (oldSession.orderIndex > session.orderIndex) {
                oldSession.orderIndex--;
            }
        }

        if (!isUserPremium(req.user)) {
            const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
            thisDeviceSessions = await getCustomRepository(SessionRepository)
                .findByUserAndDevice(req.user, session.deviceId);

            const sessionIndex = thisDeviceSessions.findIndex((thisDeviceSession: Session) => session.id === thisDeviceSession.id);
            thisDeviceSessions.splice(sessionIndex, 1);

            thisDeviceSessions.forEach((session: Session, index: number) => {
                if (typeof membershipRestrictions.sessionLimit() === 'number' && index < membershipRestrictions.sessionLimit()) {
                    session.isEnabled = true;
                }
            });
        }

        let tabsToArchive: WindowTab[] = [];
        for (const window of session.windows) {
            tabsToArchive = [...tabsToArchive, ...window.tabs];
        }

        await archiveTabsForUser(tabsToArchive, req.user);
        await getConnection().transaction(async em => {
            await em.getRepository(Session).save([...oldSessions, ...thisDeviceSessions]);
            await em.getRepository(CollaboratedSession).save(oldCollaboratedSessions);

            if (session.enabledForCollaboration) {
                const collaboratedSessions = await em.getRepository(CollaboratedSession).find({
                    where: {
                        relatedSession: session
                    },
                    relations: ['collaborator', 'collaborator.user']
                });

                for (const collaboratedSession of collaboratedSessions) {
                    if (collaboratedSession.collaborator.user) {
                        await decrementIndexesOfOtherSessions(
                            collaboratedSession.collaborator.user,
                            collaboratedSession.orderIndex
                        )
                    }
                }

                await em.getRepository(CollaboratedSession).remove(collaboratedSessions);
            }

            await em.getRepository(Session).remove(session);
        });

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    deleteAction()
];

export default _export;
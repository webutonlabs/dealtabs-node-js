import {ResolvedRequest} from "../../request/ResolvedRequest";
import {NextFunction, RequestHandler, Response} from "express";
import BadRequest from "../../response/exception/BadRequest";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {Session} from "../../../domain/entity/session/Session";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {getConnection, getCustomRepository} from "typeorm";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import SessionNormalizer from "../../response/normalizer/SessionNormalizer";
import {error} from "../../../infrustructure/service/logs/logs";

const saveSharedSessionAction = async (req: ResolvedRequest, res: Response, next: NextFunction) => {
    const sharedSession = req.resolvedParams.session;

    if (sharedSession.user.id === req.user.id) {
        return next(new BadRequest('You are the owner of this session'));
    }

    const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
    if (!membershipRestrictions.saveSharedSession()) {
        return next(new BadRequest('This feature is available only for premium users'));
    }

    const session = new Session(
        sharedSession.name,
        req.userAgent,
        req.user,
        req.deviceId
    );

    session.windows = [];

    sharedSession.windows.forEach((windowData: SessionWindow, windowIndex: number) => {
        let window = new SessionWindow(session, windowData.name);
        window.orderIndex = windowIndex;
        window.tabs = [];

        if (typeof membershipRestrictions.windowsLimit() === 'number' && windowIndex >= membershipRestrictions.windowsLimit()) {
            window.isEnabled = false;
        }

        windowData.tabs.forEach((tabData: WindowTab, tabIndex: number) => {
            let tab = new WindowTab(tabData.name, tabData.url, window, tabData.icon);
            tab.orderIndex = tabIndex;

            if (typeof membershipRestrictions.tabsLimit() === 'number' && tabIndex >= membershipRestrictions.tabsLimit()) {
                tab.isEnabled = false;
            }

            window.tabs.push(tab);
        })

        session.windows.push(window);
    })

    try {
        let oldSessions: Session[] = await getCustomRepository(SessionRepository).findByUser(req.user.id);

        for (const oldSession of oldSessions) {
            oldSession.orderIndex++;
        }

        if (false !== membershipRestrictions.sessionLimit()) {
            const thisDeviceSessions = oldSessions.filter(session => session.deviceId === req.deviceId);
            // Sorting by orderIndex in ASC
            thisDeviceSessions.sort((a, b) => a.orderIndex - b.orderIndex);

            // disabling sessions of current user device
            // which are after session limit
            for (let i = Number(membershipRestrictions.sessionLimit()) - 1; i < thisDeviceSessions.length; i++) {
                thisDeviceSessions[i].isEnabled = false;
            }

            for (let i = 0; i < oldSessions.length; i++) {
                for (let j = 0; j < thisDeviceSessions.length; j++) {
                    if (thisDeviceSessions[j].id === oldSessions[i].id) {
                        oldSessions[i].isEnabled = thisDeviceSessions[j].isEnabled;
                        break;
                    }
                }
            }
        }

        await getConnection().getRepository(Session).save([...oldSessions, session]);
    } catch (e) {
        return next(e);
    }

    res.status(200).json(SessionNormalizer.normalize(session));
}

const _export: RequestHandler[] = [
    saveSharedSessionAction
];

export default _export;
import {RequestHandler, Response} from "express";
import SessionNormalizer from "../../response/normalizer/SessionNormalizer";
import {param} from "express-validator";
import {ResolvedRequest} from "../../request/ResolvedRequest";

const sanitization: RequestHandler[] = [
    param('id').trim(),
]

const viewAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        res.status(200).json(SessionNormalizer.normalize(req.resolvedParams.session));
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    viewAction()
]

export default _export;
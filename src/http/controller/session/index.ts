import viewAction from "./view";
import createAction from "./create";
import deleteAction from "./delete";
import listAction from "./list";
import editAction from "./edit";
import orderAction from "./order";
import createSharedAction from "./createShared";
import orderWindowsAction from "./orderWindows";
import viewBySharedReferenceId from "./viewBySharedReferenceId";
import saveSharedSession from "./saveSharedSession";
import {Session} from "../../../domain/entity/session/Session";
import {getConnection} from "typeorm";
import NotFound from "../../response/exception/NotFound";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {NextFunction, Response} from "express";
import {SharedSession} from "../../../domain/entity/session/SharedSession";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {checkCollaboratorSessionAccess} from "../../../infrustructure/security/checkCollaboratorSessionAccess";

export namespace ParamHandler {
    export const id = async (req: ResolvedRequest, res: Response, next: NextFunction, value: any) => {
        try {
            const session = await getConnection()
                .getRepository(Session)
                .findOne(value, {
                    relations: ['user', 'windows', 'windows.tabs']
                });

            if (undefined === session) {
                return next(new NotFound('Session was not found'));
            }

            if (session.user.id !== req.user.id) {
                try {
                    await checkCollaboratorSessionAccess(req.user.email, session.id);
                } catch (e) {
                    return next(e);
                }
            }

            session.windows = session.windows.sort((a: SessionWindow, b: SessionWindow) => {
                return a.orderIndex - b.orderIndex;
            });

            for (const window of session.windows) {
                window.tabs = window.tabs.sort((a: WindowTab, b: WindowTab) => {
                    return a.orderIndex - b.orderIndex;
                });
            }

            req.resolvedParams.session = session;

            return next();
        } catch (e) {
            return next(e);
        }
    };

    export const referenceId = async (req: ResolvedRequest, res: Response, next: NextFunction, value: any) => {
        try {
            const sharedSession = await getConnection().getRepository(SharedSession).findOne(
                {
                    where: {
                        referenceId: value
                    },
                    relations: [
                        'session',
                        'session.windows',
                        'session.windows.tabs',
                        'session.user',
                        'session.user.membership'
                    ]
                }
            );

            if (undefined === sharedSession) {
                throw new Error('Session was not found');
            }

            if (sharedSession.session.isEnabled === false) {
                throw new Error('Session is disabled');
            }

            req.resolvedParams.session = sharedSession.session;

            return next();
        } catch (e) {
            return next(e);
        }
    };
}

export {
    viewAction,
    createAction,
    deleteAction,
    listAction,
    editAction,
    orderAction,
    createSharedAction,
    orderWindowsAction,
    viewBySharedReferenceId,
    saveSharedSession
}

import {RequestHandler, Response} from "express";
import {SharedSession} from "../../../domain/entity/session/SharedSession";
import {v4 as uuid} from "uuid";
import {getConnection} from "typeorm";
import {SharedSessionNormalizer} from "../../response/normalizer/SharedSessionNormalizer";
import {ResolvedRequest} from "../../request/ResolvedRequest";

const createSharedAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        let sharedSession = await getConnection().getRepository(SharedSession).findOne({
            where: {
                session: req.resolvedParams.session.id
            },
            relations: ['session']
        });

        if (undefined === sharedSession) {
            sharedSession = new SharedSession(req.resolvedParams.session, uuid());
            await getConnection().getRepository(SharedSession).save(sharedSession);
        }

        res.status(200).json(SharedSessionNormalizer.normalize(sharedSession));
    }
}

const _export = [
    createSharedAction()
];

export default _export;
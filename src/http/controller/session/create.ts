import {NextFunction, RequestHandler, Response} from "express";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {Session} from "../../../domain/entity/session/Session";
import {getConnection, getCustomRepository} from "typeorm";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import SessionNormalizer from "../../response/normalizer/SessionNormalizer";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {error} from "../../../infrustructure/service/logs/logs";

interface TabData {
    name: string,
    icon?: string,
    url: string
}

interface WindowData {
    name?: string,
    tabs: TabData[]
}

const sanitization: RequestHandler[] = [
    body('name').trim().escape(),
    body('windows.*.tabs.*.name').trim(),
    body('windows.*.tabs.*.url').trim(),
    body('windows.*.tabs.*.icon').trim()
]

const validation: RequestHandler[] = [
    body('name').notEmpty().withMessage('Name cannot be empty'),
    body('windows.*.name').optional(),
    body('windows.*.tabs.*.name').notEmpty().withMessage('Tab name cannot be empty'),
    body('windows.*.tabs.*.url').notEmpty().withMessage('Tab url cannot be empty')
];

const createAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);

        let session = new Session(
            req.body.name,
            req.userAgent,
            req.user,
            req.deviceId
        );

        session.windows = [];

        req.body.windows.forEach((windowData: WindowData, windowIndex: number) => {
            let window = new SessionWindow(session, windowData.name);
            window.orderIndex = windowIndex;
            window.tabs = [];

            if (typeof membershipRestrictions.windowsLimit() === 'number' && windowIndex >= membershipRestrictions.windowsLimit()) {
                window.isEnabled = false;
            }

            windowData.tabs.forEach((tabData: TabData, tabIndex: number) => {
                let tab = new WindowTab(tabData.name, tabData.url, window, tabData.icon);
                tab.orderIndex = tabIndex;

                if (typeof membershipRestrictions.tabsLimit() === 'number' && tabIndex >= membershipRestrictions.tabsLimit()) {
                    tab.isEnabled = false;
                }

                window.tabs.push(tab);
            })

            session.windows.push(window);
        })

        try {
            let oldSessions: Session[] = await getCustomRepository(SessionRepository).findByUser(req.user.id);
            let oldCollaboratedSessions: CollaboratedSession[] = await
                getCustomRepository(CollaboratedSessionRepository).listByUser(req.user);

            for (const oldSession of [...oldSessions, ...oldCollaboratedSessions]) {
                oldSession.orderIndex++;
            }

            if (false !== membershipRestrictions.sessionLimit()) {
                const thisDeviceSessions = oldSessions.filter(session => session.deviceId === req.deviceId);
                // Sorting by orderIndex in ASC
                thisDeviceSessions.sort((a, b) => a.orderIndex - b.orderIndex);

                // disabling sessions of current user device
                // which are after session limit
                for (let i = Number(membershipRestrictions.sessionLimit()) - 1; i < thisDeviceSessions.length; i++) {
                    thisDeviceSessions[i].isEnabled = false;
                }

                for (let i = 0; i < oldSessions.length; i++) {
                    for (let j = 0; j < thisDeviceSessions.length; j++) {
                        if (thisDeviceSessions[j].id === oldSessions[i].id) {
                            oldSessions[i].isEnabled = thisDeviceSessions[j].isEnabled;
                            break;
                        }
                    }
                }
            }

            await getConnection().transaction(async em => {
                await em.getRepository(Session).save([...oldSessions, session]);
                await em.getRepository(CollaboratedSession).save(oldCollaboratedSessions);
            })
        } catch (e) {
            return next(e);
        }

        res.status(200).json(SessionNormalizer.normalize(session));
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    createAction()
]

export default _export;
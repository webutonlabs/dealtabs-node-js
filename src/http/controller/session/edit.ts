import {RequestHandler, Response} from "express";
import {validationHandler} from "../../request/validation/validationHandler";
import {body, matchedData} from "express-validator";
import {getConnection} from "typeorm";
import {Session} from "../../../domain/entity/session/Session";
import SessionNormalizer from "../../response/normalizer/SessionNormalizer";
import {ResolvedRequest} from "../../request/ResolvedRequest";

const sanitization: RequestHandler[] = [
    body('name').trim()
];

const validation: RequestHandler[] = [
    body('name').optional().notEmpty().withMessage('Name cannot be empty')
];

const editAction = (): RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const session = req.resolvedParams.session;

        const data: Partial<Session> = matchedData(req, {locations: ['body']});
        Object.assign(session, data);

        await getConnection().getRepository(Session).save(session);

        session.user = undefined;
        res.status(200).json(SessionNormalizer.normalize(session));
    }
}

const _export = [
    ...sanitization,
    ...validation,
    validationHandler(),
    editAction()
];

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {getConnection} from "typeorm";
import {Session} from "../../../domain/entity/session/Session";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import AuthError from "../../response/exception/AuthError";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {CollaboratorRepository} from "../../../domain/repository/CollaboratorRepository";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";

const validation: RequestHandler[] = [
    body('sessions.*').isInt().withMessage('Session id must be integer')
]

const orderAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        try {
            const collaborator = await getConnection()
                .getCustomRepository(CollaboratorRepository).findByEmail(req.user.email);

            let sessions = await getConnection()
                .getRepository(Session).findByIds(req.body.sessions, {relations: ['user']});

            let indexes = [];
            let mappedCollaboratedSessionsToRelatedSession = {};
            for (const session of sessions) {
                if (session.user.id !== req.user.id) {
                    if (undefined === collaborator) {
                        return next(new AuthError('Access denied.'));
                    }

                    try {
                        const collaboratedSession = await getConnection()
                            .getCustomRepository(CollaboratedSessionRepository)
                            .findByCollaboratorAndSessionOrThrow(collaborator, session.id);

                        // @ts-ignore
                        mappedCollaboratedSessionsToRelatedSession[session.id] = collaboratedSession;
                        indexes.push(collaboratedSession.orderIndex);

                        continue;
                    } catch (e) {
                        return next(new AuthError('Access denied.'));
                    }
                }

                indexes.push(session.orderIndex);
            }

            indexes.sort();

            let newOrder = {};
            for (let i = 0; i < sessions.length; i++) {
                // @ts-ignore
                newOrder[req.body.sessions[i]] = indexes[i];
            }

            for (const session of sessions) {
                for (const [sessionId, newIndex] of Object.entries(newOrder)) {
                    if (session.id === Number(sessionId)) {
                        if (session.user.id === req.user.id) {
                            session.orderIndex = Number(newIndex);
                        } else { // not owner => collaborator
                            // @ts-ignore
                            mappedCollaboratedSessionsToRelatedSession[sessionId].orderIndex = Number(newIndex);
                        }
                    }
                }
            }

            await getConnection().transaction(async em => {
                await em.getRepository(Session).save(sessions);
                await em.getRepository(CollaboratedSession).save(Object.values(mappedCollaboratedSessionsToRelatedSession));
            })

            res.status(204).json();
        } catch (e) {
            return next(e);
        }
    }
}

const _export = [
    ...validation,
    validationHandler(),
    orderAction()
]

export default _export;
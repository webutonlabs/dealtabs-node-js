import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";

const listAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        await notificationsClient().get(`/notifications?scopes=${req.query.scopes}&user_id=${req.user.id}`)
            .then(notifications => {
                res.status(200).json(notifications.data);
            }).catch(err => {
                //
            });
    }
}

const _export = [
    listAction()
]

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {error} from "../../../infrustructure/service/logs/logs";

const readAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        await notificationsClient().patch(`/text-notifications/${req.params.id}/read`)
            .then(() => {
                res.status(204).json({});})
            .catch(err => {
                error(err.message, {user_id: req.user.id, stack: err.stack})
            });
    }
}

const _export = [
    readAction()
]

export default _export;
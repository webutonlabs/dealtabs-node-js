import {NextFunction, RequestHandler, Response} from "express";
import {body, matchedData} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {getConnection, getCustomRepository} from "typeorm";
import {WindowTabNormalizer} from "../../response/normalizer/WindowTabNormalizer";
import NotFound from "../../response/exception/NotFound";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import AuthError from "../../response/exception/AuthError";
import {SessionWindowRepository} from "../../../domain/repository/SessionWindowRepository";
import BadRequest from "../../response/exception/BadRequest";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('name').optional().trim(),
    body('url').optional().trim(),
    body('icon').optional().trim(),
    body('window').optional().trim()
];

const validation: RequestHandler[] = [
    body('name').optional().notEmpty().withMessage('Name cannot be empty'),
    body('url').optional().notEmpty().withMessage('Name cannot be empty'),
    body('icon').optional(),
    body('window').optional()
];

const editAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const windowTab = req.resolvedParams.windowTab;
        const data: Partial<WindowTab> = matchedData(req, {locations: ['body']});

        try {
            let oldWindowTabs: WindowTab[] = [];

            if (data.hasOwnProperty('window')) {
                const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
                const newSessionWindow = await getCustomRepository(SessionWindowRepository).findById(Number(data.window));

                if (undefined === newSessionWindow) {
                    throw new NotFound('Session window was not found');
                }

                if (newSessionWindow.session.user.id !== req.user.id) {
                    throw new AuthError('Access denied.');
                }

                if (newSessionWindow.id === windowTab.window.id) {
                    throw new BadRequest('Window tab is already in given session window');
                }

                // When we move a window tab to another session window
                // we have to make indexes of other tabs of the previous session window lower
                if (windowTab.window.tabs.length > 0) {
                    // which are after current window
                    const tabIndex = windowTab.window.tabs.findIndex((tab: WindowTab) => tab.id === windowTab.id);
                    oldWindowTabs = windowTab.window.tabs.slice(tabIndex + 1);

                    for (const windowTab of oldWindowTabs) {
                        windowTab.orderIndex--;
                    }
                }

                // if there are no tabs in the new session window
                let orderIndex = 0;
                if (newSessionWindow.tabs.length > 0) {
                    // otherwise adding the window tab to the end
                    // (tabs are ordered by orderIndex in SessionRepository)
                    orderIndex = newSessionWindow.tabs[newSessionWindow.tabs.length - 1].orderIndex + 1;
                }

                data.isEnabled = !(typeof membershipRestrictions.tabsLimit() === 'number' && newSessionWindow.tabs.length >= membershipRestrictions.tabsLimit());
                data.orderIndex = orderIndex;
                data.window = newSessionWindow;
            }

            Object.assign(windowTab, data);
            await getConnection().getRepository(WindowTab).save([...oldWindowTabs, windowTab]);

            res.status(200).json(WindowTabNormalizer.normalize(windowTab));
        } catch (err) {
            return next(err);
        }
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    editAction()
];

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {getConnection, getCustomRepository} from "typeorm";
import {SessionWindowRepository} from "../../../domain/repository/SessionWindowRepository";
import NotFound from "../../response/exception/NotFound";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {WindowTabNormalizer} from "../../response/normalizer/WindowTabNormalizer";
import {checkCollaboratorSessionAccess} from "../../../infrustructure/security/checkCollaboratorSessionAccess";

const sanitization: RequestHandler[] = [
    body('tabs.*.name').trim(),
    body('tabs.*.url').trim(),
    body('tabs.*.icon').optional().trim()
]

const validation: RequestHandler[] = [
    body('window').isInt(),
    body('tabs.*.name').notEmpty().withMessage('Tab name cannot be empty'),
    body('tabs.*.url').notEmpty().withMessage('Tab url cannot be empty'),
    body('tabs.*.icon').optional()
];

const createMultipleAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        const sessionWindow = await getCustomRepository(SessionWindowRepository).findById(req.body.window);

        if (undefined === sessionWindow) {
            return next(new NotFound('Session window was not found'));
        }

        if (req.user.id !== sessionWindow.session.user.id) {
            try {
                await checkCollaboratorSessionAccess(req.user.email, sessionWindow.session.id);
            } catch (e) {
                return next(e);
            }
        }

        const oldTabs: WindowTab[] = sessionWindow.tabs;

        let lastOldTabIndex = -1;
        if (oldTabs.length > 0) {
            lastOldTabIndex = oldTabs[oldTabs.length - 1].orderIndex;
        }

        let tabsCounter = oldTabs.length;
        let tabs = [];
        for (const tabData of req.body.tabs) {
            let tab = new WindowTab(tabData.name, tabData.url, sessionWindow, tabData.icon);
            tab.orderIndex = ++lastOldTabIndex;

            if (typeof membershipRestrictions.tabsLimit() === 'number' && tabsCounter >= membershipRestrictions.tabsLimit()) {
                tab.isEnabled = false;
            }

            tabs.push(tab);
            tabsCounter++;
        }

        await getConnection().getRepository(WindowTab).save(tabs);

        res.status(200).json(WindowTabNormalizer.normalizeMany(tabs));
    }
}

const _export : RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    createMultipleAction()
];

export default _export;
import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {getConnection, getCustomRepository} from "typeorm";
import {WindowTabRepository} from "../../../domain/repository/WindowTabRepository";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {archiveTabsForUser} from "../../../infrustructure/service/archiveTabs/archiveTabs";

const deleteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        const windowTab: WindowTab = req.resolvedParams.windowTab;
        const oldTabs: WindowTab[] = await getCustomRepository(WindowTabRepository)
            .findByWindowAndOrderIndex(windowTab.window.id, windowTab.orderIndex);

        for (const oldTab of oldTabs) {
            oldTab.orderIndex--;

            // enabling old tab in case if it is on the border of tabs limit
            if (typeof membershipRestrictions.tabsLimit() === 'number' && oldTab.orderIndex < membershipRestrictions.tabsLimit()) {
                oldTab.isEnabled = true;
            }
        }

        await archiveTabsForUser([windowTab], req.user);
        await getConnection().transaction(async em => {
            await em.getRepository(WindowTab).remove(windowTab);
            await em.getRepository(WindowTab).save(oldTabs);
        });

        res.status(204).json();
    }
}

const _export : RequestHandler[] = [
    deleteAction()
];

export default _export;
import createMultipleAction from './createMultiple';
import deleteAction from './delete';
import editAction from './edit';
import {getConnection} from "typeorm";
import NotFound from "../../response/exception/NotFound";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {NextFunction, Response} from "express";
import {checkCollaboratorSessionAccess} from "../../../infrustructure/security/checkCollaboratorSessionAccess";

export namespace ParamHandler {
    export const id = async (req: ResolvedRequest, res: Response, next: NextFunction, value: any) => {
        try {
            const windowTab = await getConnection()
                .getRepository(WindowTab)
                .findOne(value, {
                    relations: [
                        'window',
                        'window.session',
                        'window.session.user',
                        'window.tabs'
                    ]
                });

            if (undefined === windowTab) {
                return next(new NotFound('Window tab was not found'));
            }

            if (windowTab.window.session.user.id !== req.user.id) {
                try {
                    await checkCollaboratorSessionAccess(req.user.email, windowTab.window.session.id);
                } catch (e) {
                    return next(e);
                }
            }

            req.resolvedParams.windowTab = windowTab;

            return next();
        } catch (e) {
            return next(e);
        }
    };
}

export {
    createMultipleAction,
    deleteAction,
    editAction
}
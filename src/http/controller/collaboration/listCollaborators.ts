import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {query} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import NotFound from "../../response/exception/NotFound";
import AuthError from "../../response/exception/AuthError";
import {User} from "../../../domain/entity/user/User";
import {Collaborator} from "../../../domain/entity/collaboration/Collaborator";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    query('session').trim()
]

const validation: RequestHandler[] = [
    query('session').isInt()
]

const listAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        try {
            const session = await getConnection()
                .getCustomRepository(SessionRepository).findById(Number(req.query.session));

            if (undefined === session) {
                throw new NotFound('Session was not found');
            }

            if (!session.enabledForCollaboration) {
                throw new AuthError('Session is disabled for collaboration');
            }

            const collaboratedSessions = await getConnection().getRepository(CollaboratedSession).find({
                where: {
                    relatedSession: session,
                    isActive: true
                },
                relations: ['relatedSession', 'owner', 'collaborator', 'collaborator.user']
            });

            let normalizedCollaborators = [];
            let allowedUserIds = [session.user.id]; // owner

            for (const session of collaboratedSessions) {
                if (session.collaborator.user) {
                    allowedUserIds.push(session.collaborator.user.id);
                }

                normalizedCollaborators.push(normalizeCollaborator(session.collaborator, session));
            }

            if (!allowedUserIds.includes(req.user.id)) {
                throw new AuthError('Access denied.');
            }

            res.status(200).json([normalizeOwner(session.user), ...normalizedCollaborators]);
        } catch (e) {
            return next(e);
        }
    }
}

const normalizeOwner = (user: User) => {
    return {
        email: user.email,
        is_confirmed: true,
        is_owner: true
    }
}

const normalizeCollaborator = (collaborator: Collaborator, collaboratedSession: CollaboratedSession) => {
    return {
        email: collaborator.user ? collaborator.user.email : collaborator.email,
        is_confirmed: collaboratedSession.isConfirmed,
        is_owner: false
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    listAction()
];

export default _export;
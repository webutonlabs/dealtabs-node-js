import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import AuthError from "../../response/exception/AuthError";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {Collaborator} from "../../../domain/entity/collaboration/Collaborator";
import BadRequest from "../../response/exception/BadRequest";
import {Session} from "../../../domain/entity/session/Session";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {CollaboratorRepository} from "../../../domain/repository/CollaboratorRepository";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {translator} from "../../../infrustructure/service/translation/translator";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('session').trim()
]

const validation: RequestHandler[] = [
    body('session').isInt()
]

const confirmAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        try {
            const collaborator = await getConnection()
                .getCustomRepository(CollaboratorRepository).findByEmailOrThrow(req.user.email);

            const collaboratedSession = await getConnection()
                .getCustomRepository(CollaboratedSessionRepository)
                .findByCollaboratorAndSessionOrThrow(collaborator, req.body.session);

            if (!collaboratedSession.relatedSession.enabledForCollaboration) {
                throw new AuthError('Session is disabled for collaboration');
            }

            if (collaboratedSession.isConfirmed) {
                throw new BadRequest('Invitation is already confirmed');
            }

            if (collaboratedSession.collaborator.user) {
                 if (req.user.id !== collaboratedSession.collaborator.user.id) {
                     throw new AuthError('Access denied.');
                 }

                 collaboratedSession.isConfirmed = true;
            } else { // case when not registered user was invited
                if (req.user.email !== collaboratedSession.collaborator.email) {
                    throw new AuthError('Access denied.');
                }

                collaboratedSession.isConfirmed = true;
                collaboratedSession.collaborator.user = req.user;
            }

            // user may have sessions
            // do this for correct sort sessions logic
            const oldSessions: Session[] = await getConnection()
                .getCustomRepository(SessionRepository).findByUser(collaborator.user.id);
            const oldCollaboratedSessions: CollaboratedSession[] = await getConnection()
                .getCustomRepository(CollaboratedSessionRepository)
                .listByUser(collaborator.user);

            for (const oldSession of [...oldSessions, ...oldCollaboratedSessions]) {
                oldSession.orderIndex++;
            }

            await getConnection().transaction(async em => {
                await em.getRepository(CollaboratedSession).save([collaboratedSession, ...oldCollaboratedSessions]);
                await em.getRepository(Collaborator).save(collaboratedSession.collaborator);
                await em.getRepository(Session).save(oldSessions);
            });

            notificationsClient().post('/notifications', {
                type: 'text',
                context: {
                    user: {
                        id: req.user.id
                    },
                    content: `${collaboratedSession.relatedSession.name} ${translator(req.user.locale).trans('was-added-your-sessions-list')}`
                }
            }).then(() => {
                //
            }).catch(err => {
                error(err.message, {user_id: req.user.id, stack: err.stack})
            });
        } catch (e) {
            return next(e);
        }

        res.status(204).json({});
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    confirmAction()
];

export default _export;
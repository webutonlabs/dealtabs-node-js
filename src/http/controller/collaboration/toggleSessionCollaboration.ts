import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {Session} from "../../../domain/entity/session/Session";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import NotFound from "../../response/exception/NotFound";
import AuthError from "../../response/exception/AuthError";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('session').trim()
]

const validation: RequestHandler[] = [
    body('session').isInt()
]

const toggleAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        try {
            const session = await getConnection()
                .getCustomRepository(SessionRepository).findById(req.body.session);

            if (session === undefined) {
                throw new NotFound('Session was not found');
            }

            if (req.user.id !== session.user.id) {
                throw new AuthError('Access denied.');
            }

            session.enabledForCollaboration = !session.enabledForCollaboration;

            const collaboratedSessions = await getConnection().getRepository(CollaboratedSession).find({
                where: {
                    relatedSession: session
                }
            });

            for (const collaboratedSession of collaboratedSessions) {
                session.enabledForCollaboration
                    ? (collaboratedSession.isActive = true)
                    : (collaboratedSession.isActive = false)
            }

            await getConnection().transaction(async em => {
                await em.getRepository(Session).save(session);
                await em.getRepository(CollaboratedSession).save(collaboratedSessions);
            });

            res.status(204).json({});
        } catch (e) {
            return next(e);
        }
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    toggleAction()
];

export default _export;
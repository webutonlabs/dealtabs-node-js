import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {getConnection} from "typeorm";
import {User} from "../../../domain/entity/user/User";
import {Collaborator} from "../../../domain/entity/collaboration/Collaborator";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {notificationsEmailClient} from "../../../infrustructure/externalService/notificationService/notificationsEmailClient";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import AuthError from "../../response/exception/AuthError";
import BadRequest from "../../response/exception/BadRequest";
import {CollaboratorRepository} from "../../../domain/repository/CollaboratorRepository";
import NotFound from "../../response/exception/NotFound";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {translator} from "../../../infrustructure/service/translation/translator";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('email').trim(),
    body('session').trim(),
    body('invite_not_registered').optional()
]

const validation: RequestHandler[] = [
    body('email').isEmail(),
    body('session').isInt(),
    body('invite_not_registered').optional().isBoolean()
]

const inviteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        try {
            const invitationUser = await getConnection().getRepository(User).findOne({
                where: {
                    email: req.body.email
                }
            });
            const session = await getConnection()
                .getCustomRepository(SessionRepository).findById(req.body.session);

            if (session === undefined) {
                throw new NotFound('Session was not found');
            }

            if (!session.enabledForCollaboration) {
                throw new AuthError('Session is disabled for collaboration');
            }

            if (invitationUser !== undefined && invitationUser.id === session.user.id) {
                throw new BadRequest('You are the owner of this session');
            }

            // user is not owner
            if (req.user.id !== session.user.id) {
                const collaboratedSession = await getConnection()
                    .getCustomRepository(CollaboratedSessionRepository)
                    .findByCollaboratedUserId(req.user.id);

                if (undefined === collaboratedSession) { // only owner or collaborator can invite
                    throw new AuthError('Access denied.');
                }
            } else { // session can be shared if owner is on paid subscription
                const membershipRestrictions = new MembershipRestrictions().forUser(req.user);

                if (!membershipRestrictions.collaboration()) {
                    throw new AuthError('Feature is not available for your subscription');
                }
            }

            let collaborator: Collaborator = null;
            if (undefined === invitationUser) {
                if (req.body['invite_not_registered']) {
                    collaborator = await getConnection()
                        .getCustomRepository(CollaboratorRepository).findByEmailOrThrow(req.body.email);

                    if (undefined === collaborator) {
                        collaborator = new Collaborator(null, req.body.email);
                    }
                } else {
                    res.status(404).json(await getSimilarEmails(req.body.email, req.user.email));
                    return;
                }
            } else {
                collaborator = await getConnection()
                    .getCustomRepository(CollaboratorRepository).findByEmailOrThrow(req.body.email);

                if (undefined === collaborator) {
                    collaborator = new Collaborator(invitationUser);
                }
            }

            if (collaborator !== null) {
                let collaboratedSession = await getConnection().getRepository(CollaboratedSession).findOne({
                    where: {
                        relatedSession: session,
                        collaborator: collaborator
                    },
                    relations: ['owner', 'relatedSession', 'collaborator', 'collaborator.user']
                });

                if (undefined !== collaboratedSession) {
                    collaborator.user
                        ? sendInvitation(collaboratedSession, collaborator.user.locale)
                        : sendNotRegisteredInvitation(collaboratedSession, req.user.locale);

                    res.status(200).json({msg: 'Invitation email was resent'});
                    return ;
                }

                collaboratedSession = new CollaboratedSession(session, collaborator, session.user);

                await getConnection().transaction(async em => {
                    await em.getRepository(Collaborator).save(collaborator);
                    await em.getRepository(CollaboratedSession).save(collaboratedSession);
                })

                collaborator.user
                ? sendInvitation(collaboratedSession, collaborator.user.locale)
                : sendNotRegisteredInvitation(collaboratedSession, req.user.locale);

                res.status(204).json({});
            } else {
                throw new BadRequest();
            }
        } catch (e) {
            return next(e);
        }
    }
}

const getSimilarEmails = async (email: string, userEmail: string) => {
    const similarEmails = await getConnection().createEntityManager()
        .query(`SELECT email FROM users WHERE SIMILARITY(email, '${email}') > 0.6;`)

    let emails: string[] = [];
    for (const rawEmail of similarEmails) {
        if (rawEmail['email'] !== userEmail) {
            emails.push(rawEmail['email']);
        }
    }

    return emails;
}

const sendNotRegisteredInvitation = (collaboratedSession: CollaboratedSession, locale: string) => {
    const emailClient = notificationsEmailClient(locale);
    emailClient.post('/collaboration-not-registered-invitation', {
        owner: {
            name: collaboratedSession.owner.name,
            surname: collaboratedSession.owner.surname,
            email: collaboratedSession.owner.email
        },
        collaborated_session: {
            id: collaboratedSession.id,
            related_session: {
                id: collaboratedSession.relatedSession.id,
                name: collaboratedSession.relatedSession.name
            }
        },
        collaborator: {
            email: collaboratedSession.collaborator.email
        }
    }).then((res) => {
        //
    }).catch(err => {
        error(err.message, {user_id: collaboratedSession.owner.id, stack: err.stack})
    })
}

const sendInvitation = (collaboratedSession: CollaboratedSession, locale: string) => {
    const emailClient = notificationsEmailClient(locale);
    emailClient.post('/collaboration-invitation', {
        owner: {
            name: collaboratedSession.owner.name,
            surname: collaboratedSession.owner.surname,
            email: collaboratedSession.owner.email
        },
        collaborated_session: {
            id: collaboratedSession.id,
            related_session: {
                id: collaboratedSession.relatedSession.id,
                name: collaboratedSession.relatedSession.name
            }
        },
        collaborator: {
            email: collaboratedSession.collaborator.user.email,
            name: collaboratedSession.collaborator.user.name
        }
    }).then((res) => {
        //
    }).catch(err => {
        error(err.message, {user_id: collaboratedSession.owner.id, stack: err.stack})
    });

    notificationsClient().post('/notifications', {
        type: 'text',
        context: {
            user: {
                id: collaboratedSession.collaborator.user.id
            },
            content: `${translator(collaboratedSession.collaborator.user.locale).trans('were-invited-to')} ${collaboratedSession.relatedSession.name}`
        }
    }).then(() => {
        //
    }).catch(err => {
        error(err.message, {user_id: collaboratedSession.owner.id, stack: err.stack})
    });
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    inviteAction()
];

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {getConnection} from "typeorm";
import NotFound from "../../response/exception/NotFound";
import BadRequest from "../../response/exception/BadRequest";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import AuthError from "../../response/exception/AuthError";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";
import {CollaboratorRepository} from "../../../domain/repository/CollaboratorRepository";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {decrementIndexesOfOtherSessions} from "../../../infrustructure/service/sorting/orderIndexHelpers";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {translator} from "../../../infrustructure/service/translation/translator";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('email').trim(),
    body('session').trim()
]

const validation: RequestHandler[] = [
    body('email').isEmail(),
    body('session').isInt()
]

const deleteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        try {
            const session = await getConnection()
                .getCustomRepository(SessionRepository).findById(req.body.session);

            if (session === undefined) {
                throw new NotFound('Session was not found');
            }

            if (session.user.email === req.body.email && req.user.id === session.user.id) {
                throw new BadRequest('You can\'t remove yourself because you are the owner');
            }

            const collaborator = await getConnection()
                .getCustomRepository(CollaboratorRepository).findByEmailOrThrow(req.body.email);
            const collaboratedSession = await getConnection()
                .getCustomRepository(CollaboratedSessionRepository).findByCollaboratorAndSessionOrThrow(collaborator, session.id);

            if (!collaboratedSession.relatedSession.enabledForCollaboration) {
                throw new AuthError('Session is disabled for collaboration');
            }

            /**
             * Owner can remove everybody
             * Collaborators can remove only themselves
             */
            if (req.user.id !== session.user.id) {
                if (!collaboratedSession.isConfirmed) {
                    throw new BadRequest('Session is not confirmed');
                }

                if (collaborator.user) {
                    if (collaborator.user.email !== req.user.email) {
                        throw new AuthError('You can remove only yourself');
                    }
                } else {
                    throw new AuthError('Access denied.');
                    // case when user is not set to collaborator is not checked
                    // because not registered users cannot send requests to api
                }
            }

            await getConnection().getRepository(CollaboratedSession).remove(collaboratedSession);

            if (collaborator.user) {
                await decrementIndexesOfOtherSessions(collaborator.user, collaboratedSession.orderIndex);
            }

            notificationsClient().post('/notifications', {
                type: 'text',
                context: {
                    user: {
                        id: req.user.id
                    },
                    content: `${translator(req.user.locale).trans('were-removed-from')} ${collaboratedSession.relatedSession.name}`
                }
            }).then(() => {
                //
            }).catch(err => {
                error(err.message, {user_id: req.user.id, stack: err.stack})
            });

            res.status(204).json();
        } catch (e) {
            return next(e);
        }

    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    deleteAction()
];

export default _export;
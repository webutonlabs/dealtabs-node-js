import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection, getCustomRepository, getRepository} from "typeorm";
import {UserRepository} from "../../../domain/repository/UserRepository";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import {Session} from "../../../domain/entity/session/Session";
import {User} from "../../../domain/entity/user/User";
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";

const validateUserSessionsAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const limit = 100;
        let offset = 0;

        while (true) {
            const users = await getCustomRepository(UserRepository).findUsers(offset, limit);

            for (const user of users) {
                const membershipRestrictions = new MembershipRestrictions().forUser(user);
                let possibleDevices = [];

                const sessions = user.sessions;
                const collaboratedSessions = await getConnection()
                    .getCustomRepository(CollaboratedSessionRepository).listByUser(user);

                const userSessions = [...sessions, ...collaboratedSessions];
                userSessions.sort((a: Session, b: Session) => {
                    return a.orderIndex - b.orderIndex;
                });

                let sessionIndex = -1;
                for (const session of userSessions) {
                    // validating orderIndexes to be from 0 to ... => ex. 0,1,2,3...n
                    session.orderIndex = ++sessionIndex;

                    if (session instanceof Session) {
                        possibleDevices.push(session.deviceId);
                        // by default enabling session
                        // they will be disabled on line 47 if subscription plan needs this
                        session.isEnabled = true;

                        session.windows = session.windows.sort((a: SessionWindow, b: SessionWindow) => {
                            return a.orderIndex - b.orderIndex;
                        });

                        let windowIndex = -1;
                        for (const window of session.windows) {
                            // validating orderIndexes to be from 0 to ... => ex. 0,1,2,3...n
                            window.orderIndex = ++windowIndex;
                            window.isEnabled = true;

                            if (typeof membershipRestrictions.windowsLimit() === 'number' && window.orderIndex >= membershipRestrictions.windowsLimit()) {
                                window.isEnabled = false;
                            }

                            window.tabs = window.tabs.sort((a: WindowTab, b: WindowTab) => {
                                return a.orderIndex - b.orderIndex;
                            });

                            let tabIndex = -1;
                            for (const tab of window.tabs) {
                                // validating orderIndexes to be from 0 to ... => ex. 0,1,2,3...n
                                tab.orderIndex = ++tabIndex;
                                tab.isEnabled = true;

                                if (typeof membershipRestrictions.tabsLimit() === 'number' && tab.orderIndex >= membershipRestrictions.tabsLimit()) {
                                    tab.isEnabled = false;
                                }
                            }
                        }
                    }
                }

                await getConnection().transaction(async em => {
                    await getRepository(User).save(user);
                    await getRepository(CollaboratedSession).save(collaboratedSessions);
                    await getRepository(Session).save(sessions);
                });

                await getConnection().getRepository(User).save(user);

                if (typeof membershipRestrictions.sessionLimit() === 'number') {
                    // only unique devices
                    // the logic is to disable all session which reached the limit on every device
                    possibleDevices = possibleDevices.filter((val, index, arr) => {
                        return arr.indexOf(val) === index;
                    });

                    for (const device of possibleDevices) {
                        let thisDeviceSessions: Session[] = user.sessions.filter(session => session.deviceId === device);

                        thisDeviceSessions = thisDeviceSessions.sort((a: Session, b: Session) => {
                            return a.orderIndex - b.orderIndex;
                        })

                        thisDeviceSessions.forEach((session: Session, index: number) => {
                            if (index >= membershipRestrictions.sessionLimit()) {
                                session.isEnabled = false;

                                // if the session is disabled then all child relations are disabled too
                                for (const window of session.windows) {
                                    window.isEnabled = false;

                                    for (const tab of window.tabs) {
                                        tab.isEnabled = false;
                                    }
                                }
                            }
                        })

                        await getConnection().getRepository(Session).save(thisDeviceSessions);
                    }
                }
            }

            if (users.length === 0) {
                break;
            }

            offset += limit;
        }

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    validateUserSessionsAction()
];

export default _export;
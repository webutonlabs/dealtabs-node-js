import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";

const clearArchivedTabs = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        await getConnection().transaction(async em => {
            await em.query("DELETE FROM archived_tabs WHERE creation_date <= now() - INTERVAL '30 DAYS'");
        });

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    clearArchivedTabs()
];

export default _export;
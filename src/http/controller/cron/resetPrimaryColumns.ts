import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";

const resetPrimaryColumns = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        await getConnection().transaction(async em => {
            await em.query('TRUNCATE TABLE live_sync_tabs');
            await em.query('ALTER SEQUENCE live_sync_tabs_id_seq RESTART WITH 1');
        });

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    resetPrimaryColumns()
];

export default _export;
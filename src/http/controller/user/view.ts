import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {UserNormalizer} from "../../response/normalizer/UserNormalizer";

const viewAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        res.status(200).json(UserNormalizer.normalize(req.user));
    }
}

const _export: RequestHandler[] = [
    viewAction()
];

export default _export;
import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {body, matchedData} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {User} from "../../../domain/entity/user/User";
import {default as bcrypt} from "bcrypt";
import {getConnection} from "typeorm";
import {UserNormalizer} from "../../response/normalizer/UserNormalizer";
import {default as camelCaseKeys} from "camelcase-keys";

const sanitization: RequestHandler[] = [
    body('name').optional().trim().escape(),
    body('surname').optional().trim(),
    body('password').optional().trim(),
    body('locale').optional().trim(),
    body('email_subscription').optional().trim().toBoolean(),
    body('email').optional().normalizeEmail()
];

const validation: RequestHandler[] = [
    body('name').optional().notEmpty().withMessage('Name cannot be empty'),
    body('surname').optional(),
    body('password').optional().isLength({min: 6}),
    body('locale').optional().isLength({min: 2, max: 2}),
    body('email_subscription').optional().isBoolean(),
    body('email').optional().isEmail().custom(async email => {
        const user = await getConnection().getRepository(User).findOne({where: {email: email}});

        if (undefined !== user) {
            throw new Error('User already exists');
        }

        return true;
    })
];

const editAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        const user: User = req.user;

        let data: Partial<User> = matchedData(req, {locations: ['body']});
        data = camelCaseKeys(data);
        Object.assign(user, data);

        if (data.hasOwnProperty('password')) {
            user.password = bcrypt.hashSync(data.password, 10);
        }

        await getConnection().getRepository(User).save(user);

        res.status(200).json(UserNormalizer.normalize(user));
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    editAction()
]

export default _export;
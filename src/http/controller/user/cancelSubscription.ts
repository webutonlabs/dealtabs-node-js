import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {billingServiceClient} from "../../../infrustructure/externalService/billingService/billingServiceClient";

const cancelSubscriptionAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        await billingServiceClient(req.user.locale).patch('/cancel-subscription', {email: req.user.email}).then(response => {
            res.status(204).json({});
        }).catch(err => {
            res.status(500).json({message: err.response.data.message, resource: err.response.data.resource});
        });
    }
}

const _export: RequestHandler[] = [
    cancelSubscriptionAction()
];

export default _export;
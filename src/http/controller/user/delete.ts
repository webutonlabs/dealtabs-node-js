import {RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {User} from "../../../domain/entity/user/User";
import {Membership} from "../../../domain/entity/user/Membership";
import {notificationsClient} from "../../../infrustructure/externalService/notificationService/notificationsClient";
import {error} from "../../../infrustructure/service/logs/logs";
import {billingServiceClient} from "../../../infrustructure/externalService/billingService/billingServiceClient";

const deleteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response) => {
        notificationsClient().delete(`/notifications?scopes=warning,alert,text&user_id=${req.user.id}`)
            .then(() => {
                //
            })
            .catch(err => {
                error(err.message, {user_id: req.user.id, stack: err.stack})
            });

        billingServiceClient(req.user.locale).patch('/cancel-subscription', {email: req.user.email}).then(res => {
            //
        }).catch(err => {
            error(err.message, {user_id: req.user.id, stack: err.stack})
        })

        await getConnection().transaction(async em => {
            await em.getRepository(Membership).remove(req.user.membership);
            await em.getRepository(User).remove(req.user);
        });

        res.status(204).json();
    }
}

const _export: RequestHandler[] = [
    deleteAction()
]

export default _export;
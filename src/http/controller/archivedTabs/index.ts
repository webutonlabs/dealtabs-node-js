import deleteAction from './delete';
import listAction from './list';
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {NextFunction, Response} from "express";
import {getConnection} from "typeorm";
import NotFound from "../../response/exception/NotFound";
import AuthError from "../../response/exception/AuthError";
import {ArchivedTab} from "../../../domain/entity/archive/ArchivedTab";

export namespace ParamHandler {
    export const id = async (req: ResolvedRequest, res: Response, next: NextFunction, value: any) => {
        try {
            const archivedTab = await getConnection()
                .getRepository(ArchivedTab)
                .findOne(value, {relations: ['user']});

            if (undefined === archivedTab) {
                return next(new NotFound('Archived tab was not found'));
            }

            if (archivedTab.user.id !== req.user.id) {
                return next(new AuthError('Access denied.'));
            }

            req.resolvedParams.archivedTab = archivedTab;

            return next();
        } catch (e) {
            return next(e);
        }
    };
}

export {
    deleteAction,
    listAction
}
import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {ArchivedTab} from "../../../domain/entity/archive/ArchivedTab";
import {getConnection} from "typeorm";

const deleteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const archivedTab: ArchivedTab = req.resolvedParams.archivedTab;
        await getConnection().getRepository(ArchivedTab).remove(archivedTab);

        res.status(204).json();
    }
}

const _export = [
    deleteAction()
]

export default _export;
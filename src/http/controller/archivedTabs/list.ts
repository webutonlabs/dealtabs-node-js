import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {ArchivedTabsRepository} from "../../../domain/repository/ArchivedTabsRepository";
import {PaginationResolver} from "../../response/pagination/PaginationResolver";
import ArchivedTabNormalizer from "../../response/normalizer/ArchivedTabNormalizer";

const listAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const pagination = await getConnection()
            .getCustomRepository(ArchivedTabsRepository)
            .listForUser(req.user);

        new PaginationResolver(pagination, res)
            .setHeaders().status(200).json(ArchivedTabNormalizer.normalizeMany(pagination.data));
    }
}

const _export = [
    listAction()
]

export default _export;
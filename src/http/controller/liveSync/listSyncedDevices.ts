import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {SyncedDevice} from "../../../domain/entity/liveSync/SyncedDevice";
import {SyncedDeviceNormalizer} from "../../response/normalizer/liveSync/SyncedDeviceNormalizer";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import AuthError from "../../response/exception/AuthError";
import {SyncedTab} from "../../../domain/entity/liveSync/SyncedTab";

const listAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        if (!membershipRestrictions.liveSync()) {
            return next(new AuthError('Access denied.'));
        }

        const syncedDevices = await getConnection().getRepository(SyncedDevice).find({
            where: {
                user: req.user,
                isSyncEnabled: true
            },
            relations: ['syncedTabs']
        });

        /**
         * In some cases there are duplicates of SyncedTab in db.
         * There is an uniqueness identifier of them => orderIndex.
         * To return only unique SyncedTabs we filter them by this identifier.
         */
        for (const device of syncedDevices) {
            let uniqueTabs: SyncedTab[] = [];
            for (const tab of device.syncedTabs) {
                const isTabInArray = uniqueTabs.some((syncedTab) => {
                    return syncedTab.orderIndex === tab.orderIndex;
                });

                if (!isTabInArray) {
                    uniqueTabs.push(tab);
                }
            }

            device.syncedTabs = uniqueTabs;
        }

        res.status(200).json(SyncedDeviceNormalizer.normalizeMany(syncedDevices));
    }
}

const _export: RequestHandler[] = [
    listAction()
];

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {SyncedDevice} from "../../../domain/entity/liveSync/SyncedDevice";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import AuthError from "../../response/exception/AuthError";
import NotFound from "../../response/exception/NotFound";
import {error} from "../../../infrustructure/service/logs/logs";

const deleteAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        if (!membershipRestrictions.liveSync()) {
            return next(new AuthError('Access denied.'));
        }

        try {
            let syncedDevice = await getConnection().getRepository(SyncedDevice).findOne({
                where: {
                    deviceId: req.deviceId,
                    user: req.user // multiple accounts can be used on same device
                }
            });

            if (undefined === syncedDevice) {
                throw new NotFound('Device was not found');
            }

            await getConnection().getRepository(SyncedDevice).remove(syncedDevice);

            res.status(204).json({});
        } catch (err) {
            return next(err);
        }
    }
}

const _export: RequestHandler[] = [
    deleteAction()
];

export default _export;
import editSyncedDevice from './editSyncedDevice';
import replaceSyncedDeviceTabs from './replaceSyncedDeviceTabs';
import viewSyncedDevice from './viewSyncedDevice';
import listSyncedDevices from "./listSyncedDevices";
import deleteSyncedDevice from './deleteSyncedDevice';

export {
    editSyncedDevice,
    replaceSyncedDeviceTabs,
    viewSyncedDevice,
    listSyncedDevices,
    deleteSyncedDevice
}
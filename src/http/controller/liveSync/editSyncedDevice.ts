import {NextFunction, RequestHandler, Response} from "express";
import {body, matchedData} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {SyncedDevice} from "../../../domain/entity/liveSync/SyncedDevice";
import NotFound from "../../response/exception/NotFound";
import {SyncedDeviceNormalizer} from "../../response/normalizer/liveSync/SyncedDeviceNormalizer";
import {default as camelCaseKeys} from "camelcase-keys";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import AuthError from "../../response/exception/AuthError";

const sanitization: RequestHandler[] = [
    body('name').optional().trim(),
    body('is_sync_enabled').optional(),
];

const validation: RequestHandler[] = [
    body('name').optional().notEmpty().withMessage('Name cannot be empty'),
    body('is_sync_enabled').optional().isBoolean()
];

const editAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        if (!membershipRestrictions.liveSync()) {
            return next(new AuthError('Access denied.'));
        }

        const syncedDevice = await getConnection().getRepository(SyncedDevice).findOne({
            where: {
                deviceId: req.deviceId,
                user: req.user // multiple accounts can be used on same device
            }
        });

        if (undefined === syncedDevice) {
            return next(new NotFound('Device was not found'));
        }

        try {
            let data: Partial<SyncedDevice> = matchedData(req, {locations: ['body']});
            data = camelCaseKeys(data);
            Object.assign(syncedDevice, data);

            await getConnection().getRepository(SyncedDevice).save(syncedDevice);

            res.status(200).json(SyncedDeviceNormalizer.normalize(syncedDevice));
        } catch (err) {
            return next(err);
        }
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    editAction()
];

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {SyncedDevice} from "../../../domain/entity/liveSync/SyncedDevice";
import {SyncedDeviceNormalizer} from "../../response/normalizer/liveSync/SyncedDeviceNormalizer";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import AuthError from "../../response/exception/AuthError";
import {error} from "../../../infrustructure/service/logs/logs";

const viewAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        if (!membershipRestrictions.liveSync()) {
            return next(new AuthError('Access denied.'));
        }

        let syncedDevice = await getConnection().getRepository(SyncedDevice).findOne({
            where: {
                deviceId: req.deviceId,
                user: req.user // multiple accounts can be used on same device
            }
        });

        try {
            if (undefined === syncedDevice) {
                syncedDevice = new SyncedDevice(
                    req.user,
                    req.userAgent,
                    req.deviceId
                );

                await getConnection().getRepository(SyncedDevice).save(syncedDevice);
            }

            res.status(200).json(SyncedDeviceNormalizer.normalize(syncedDevice));
        } catch (err) {
            return next(err);
        }
    }
}

const _export: RequestHandler[] = [
    viewAction()
];

export default _export;
import {NextFunction, RequestHandler, Response} from "express";
import {body} from "express-validator";
import {validationHandler} from "../../request/validation/validationHandler";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {getConnection} from "typeorm";
import {SyncedDevice} from "../../../domain/entity/liveSync/SyncedDevice";
import NotFound from "../../response/exception/NotFound";
import {SyncedTab} from "../../../domain/entity/liveSync/SyncedTab";
import {MembershipRestrictions} from "../../../infrustructure/membership/MembershipRestrictions";
import AuthError from "../../response/exception/AuthError";
import {error} from "../../../infrustructure/service/logs/logs";

const sanitization: RequestHandler[] = [
    body('tabs.*.name').trim(),
    body('tabs.*.url').trim(),
    body('tabs.*.icon').optional().trim()
]

const validation: RequestHandler[] = [
    body('tabs.*.name').notEmpty().withMessage('Tab name cannot be empty'),
    body('tabs.*.url').notEmpty().withMessage('Tab url cannot be empty'),
    body('tabs.*.icon').optional(),
    body('tabs.*.is_active').isBoolean()
];

const replaceAction = () : RequestHandler => {
    return async (req: ResolvedRequest, res: Response, next: NextFunction) => {
        const membershipRestrictions = new MembershipRestrictions().forUser(req.user);
        if (!membershipRestrictions.liveSync()) {
            return next(new AuthError('Access denied.'));
        }

        const syncedDevice = await getConnection().getRepository(SyncedDevice).findOne({
            where: {
                deviceId: req.deviceId,
                user: req.user // multiple accounts can be used on same device
            }
        });

        if (undefined === syncedDevice) {
            return next(new NotFound('Device was not found'));
        }

        const tabs = await getConnection().getRepository(SyncedTab).find({
            where: {
                syncedDevice: syncedDevice
            }
        });

        await getConnection();

        let syncedTabs: SyncedTab[] = [];
        req.body.tabs.forEach((tabData: any, index: number) => {
            syncedTabs.push(new SyncedTab(
                syncedDevice,
                tabData.name,
                tabData.url,
                index,
                tabData['is_active'],
                tabData.icon,
            ));
        })

        try {
            await getConnection().transaction(async em => {
                await em.getRepository(SyncedTab).remove(tabs);
                await em.getRepository(SyncedTab).save(syncedTabs);
            })

            res.status(204).json();
        } catch (err) {
            return next(err);
        }
    }
}

const _export: RequestHandler[] = [
    ...sanitization,
    ...validation,
    validationHandler(),
    replaceAction()
];

export default _export;
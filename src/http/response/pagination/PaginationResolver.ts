import {PaginationAwareObject} from "typeorm-pagination/dist/helpers/pagination";
import {Response} from "express";

export class PaginationResolver {
    private readonly _paginationData: PaginationAwareObject;
    private _response: Response;
    
    constructor(paginationData: PaginationAwareObject, response: Response) {
        this._paginationData = paginationData;
        this._response = response;
    }

    public setHeaders(): Response {
        this._response.setHeader('X-PAGINATION-FROM', this._paginationData.from);
        this._response.setHeader('X-PAGINATION-TO', this._paginationData.to);
        this._response.setHeader('X-PAGINATION-PER-PAGE', this._paginationData.per_page);
        this._response.setHeader('X-PAGINATION-TOTAL', this._paginationData.total);
        this._response.setHeader('X-PAGINATION-CURRENT-PAGE', this._paginationData.current_page);
        this._response.setHeader('X-PAGINATION-PREV-PAGE', this._paginationData.prev_page);
        this._response.setHeader('X-PAGINATION-NEXT-PAGE', this._paginationData.next_page);

        return this._response;
    }
}
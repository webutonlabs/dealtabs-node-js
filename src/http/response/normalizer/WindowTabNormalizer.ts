import {WindowTab} from "../../../domain/entity/session/WindowTab";

export interface NormalizedWindowTab {
    id: number,
    name: string,
    icon?: string,
    url: string,
    order_index: number,
    is_enabled: boolean,
    creation_date: Date
}

export class WindowTabNormalizer {
    static normalize(tab: WindowTab): NormalizedWindowTab {
        return {
            id: tab.id,
            name: tab.name,
            icon: tab.icon,
            url: tab.url,
            order_index: tab.orderIndex,
            is_enabled: tab.isEnabled,
            creation_date: tab.creationDate
        }
    }

    static normalizeMany(tabs: WindowTab[]): NormalizedWindowTab[] {
        let data: NormalizedWindowTab[] = [];

        tabs.forEach(tab => {
            data.push(this.normalize(tab));
        })

        return data;
    }
}
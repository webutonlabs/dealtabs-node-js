import {Session} from "../../../domain/entity/session/Session";
import SessionWindowNormalizer, {NormalizedSessionWindow} from "./SessionWindowNormalizer";
import {UserNormalizer} from "./UserNormalizer";

interface NormalizedSession {
    id: number,
    name: string,
    user_agent: string,
    creation_date: Date,
    order_index: number,
    windows: NormalizedSessionWindow[],
    is_enabled: boolean,
    user?: object,
    enabled_for_collaboration: boolean,
    device_id: string,
    [key: string]: any
}

export default class SessionNormalizer {
    static normalize(session: Session): NormalizedSession {
        let normalizedSession: NormalizedSession = {
            id: session.id,
            name: session.name,
            user_agent: session.userAgent,
            creation_date: session.creationDate,
            order_index: session.orderIndex,
            is_enabled: session.isEnabled,
            windows: [],
            owned: session.owned === undefined,
            enabled_for_collaboration: session.enabledForCollaboration,
            device_id: session.deviceId
        };

        if (undefined !== session.windows && session.windows.length > 0) {
            session.windows.forEach(window => {
                normalizedSession.windows.push(SessionWindowNormalizer.normalize(window))
            })
        }

        if (undefined !== session.user) {
            normalizedSession.user = UserNormalizer.normalize(session.user);
        }

        return normalizedSession;
    }

    static normalizeMany(sessions: Session[]): NormalizedSession[] {
        let data: NormalizedSession[] = [];

        sessions.forEach(session => {
            data.push(this.normalize(session));
        })

        return data;
    }
}
import {SharedSession} from "../../../domain/entity/session/SharedSession";

interface NormalizedSharedSession {
    id: number,
    session_id: number,
    reference_id: string
}

export class SharedSessionNormalizer {
    static normalize(session: SharedSession): NormalizedSharedSession {
        return {
            id: session.id,
            session_id: session.session.id,
            reference_id: session.referenceId
        }
    }
}
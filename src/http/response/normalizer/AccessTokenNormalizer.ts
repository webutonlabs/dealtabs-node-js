import {AccessToken} from "../../../domain/entity/user/AccessToken";

export class AccessTokenNormalizer {
    static normalize(token: AccessToken) {
        return {
            access_token: token.token,
            expires_at: token.expiresAt
        }
    }
}
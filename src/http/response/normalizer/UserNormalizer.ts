import {User} from "../../../domain/entity/user/User";

export class UserNormalizer {
    static normalize(user: User) {
        const normalizedUser = {
            id: user.id,
            name: user.name,
            surname: user.surname,
            locale: user.locale,
            is_email_confirmed: user.isEmailConfirmed,
            email: user.email,
            membership: {},
            emails_newsletter: user.emailSubscription
        };

        if (undefined !== user.membership) {
            normalizedUser.membership = {
                membership_type: user.membership.membershipType,
                    subscription_expires: user.membership.subscriptionExpires,
                    subscription_type: user.membership.subscriptionType,
                    allowed_trial: user.membership.allowedTrial,
                    cancelled_subscription: user.membership.cancelledSubscription
            }
        }

        return normalizedUser;
    }
}
import {ArchivedTab} from "../../../domain/entity/archive/ArchivedTab";

interface NormalizedArchiveTab {
    id: number,
    name: string,
    icon?: string,
    url: string,
    creation_date: Date
}

export default class ArchivedTabNormalizer {
    static normalize(archivedTab: ArchivedTab): NormalizedArchiveTab {
        return {
            id: archivedTab.id,
            name: archivedTab.name,
            url: archivedTab.url,
            icon: archivedTab.icon,
            creation_date: archivedTab.creationDate
        }
    }

    static normalizeMany(archivedTabs: ArchivedTab[]): NormalizedArchiveTab[] {
        let normalized: NormalizedArchiveTab[] = [];

        for (const archiveTab of archivedTabs) {
            normalized = [...normalized, this.normalize(archiveTab)];
        }

        return normalized;
    }
}
import {SessionWindow} from "../../../domain/entity/session/SessionWindow";
import {NormalizedWindowTab, WindowTabNormalizer} from "./WindowTabNormalizer";

export interface NormalizedSessionWindow {
    id: number,
    name?: string,
    order_index: number,
    creation_date: Date,
    is_enabled: boolean,
    tabs: NormalizedWindowTab[]
}

export default class SessionWindowNormalizer {
    static normalize(window: SessionWindow): NormalizedSessionWindow {
        let normalizedWindow: NormalizedSessionWindow = {
            id: window.id,
            name: window.name,
            order_index: window.orderIndex,
            creation_date: window.creationDate,
            is_enabled: window.isEnabled,
            tabs: []
        };

        if (undefined !== window.tabs && window.tabs.length > 0) {
            window.tabs.forEach(tab => {
                normalizedWindow.tabs.push(WindowTabNormalizer.normalize(tab));
            })
        }

        return normalizedWindow;
    }

    static normalizeMany(windows: SessionWindow[]): NormalizedSessionWindow[] {
        let data: NormalizedSessionWindow[] = [];

        windows.forEach(window => {
            data.push(this.normalize(window));
        })

        return data;
    }
}
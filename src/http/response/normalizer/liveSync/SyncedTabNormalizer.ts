import {SyncedTab} from "../../../../domain/entity/liveSync/SyncedTab";

export interface NormalizedSyncedTab {
    id: number,
    name: string,
    icon?: string,
    url: string,
    order_index: number,
    is_active: boolean
}

export class SyncedTabNormalizer {
    static normalize(tab: SyncedTab): NormalizedSyncedTab {
        return {
            id: tab.id,
            name: tab.name,
            icon: tab.icon,
            url: tab.url,
            order_index: tab.orderIndex,
            is_active: tab.isActive
        }
    }

    static normalizeMany(tabs: SyncedTab[]): NormalizedSyncedTab[] {
        let data: NormalizedSyncedTab[] = [];

        tabs.forEach(tab => {
            data.push(this.normalize(tab));
        })

        return data;
    }
}
import {SyncedDevice} from "../../../../domain/entity/liveSync/SyncedDevice";
import {NormalizedSyncedTab, SyncedTabNormalizer} from "./SyncedTabNormalizer";

interface NormalizedSyncedDevice {
    id: string,
    name: string,
    is_sync_enabled: boolean,
    tabs: NormalizedSyncedTab[]
}

export class SyncedDeviceNormalizer {
    static normalize(device: SyncedDevice): NormalizedSyncedDevice {
        let normalizedDevice: NormalizedSyncedDevice = {
            id: device.deviceId,
            name: device.name,
            is_sync_enabled: device.isSyncEnabled,
            tabs: []
        }

        if (undefined !== device.syncedTabs) {
            normalizedDevice.tabs = SyncedTabNormalizer.normalizeMany(device.syncedTabs);
        }

        return normalizedDevice;
    }

    static normalizeMany(devices: SyncedDevice[]): NormalizedSyncedDevice[] {
        let data: NormalizedSyncedDevice[] = [];

        devices.forEach(device => {
            data.push(this.normalize(device));
        })

        return data;
    }
}
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";

interface normalizedQuickTab {
    id: number,
    name: string,
    url: string,
    order_index: number,
    icon?: string,
    is_enabled: boolean,
    creation_date: Date
}

export class QuickTabNormalizer {
    static normalize(tab: QuickTab): normalizedQuickTab {
        return {
            id: tab.id,
            name: tab.name,
            icon: tab.icon,
            url: tab.url,
            order_index: tab.orderIndex,
            is_enabled: tab.isEnabled,
            creation_date: tab.creationDate
        }
    }

    static normalizeMany(tabs: QuickTab[]): normalizedQuickTab[] {
        let data: normalizedQuickTab[] = [];

        tabs.forEach(tab => {
            data.push(this.normalize(tab));
        })

        return data;
    }
}
class NotFound extends Error {
    constructor(message: string = 'Authentication error.') {
        super(message);
    }
}

export default NotFound;
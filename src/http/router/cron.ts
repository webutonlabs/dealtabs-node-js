import {Router} from "express";
import * as Cron from "../controller/cron";

const router = Router();

router.get('/validate-user-sessions', Cron.validateUserSessionsAction);
router.get('/reset-primary-columns', Cron.resetPrimaryColumns);
router.get('/clear-archived-tabs', Cron.clearArchivedTabs);

export default router;
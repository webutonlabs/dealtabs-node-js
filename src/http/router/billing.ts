import {Router} from "express";
import * as Billing from "../controller/billing";

const router = Router();

router.patch('/make-user-premium', Billing.makeUserPremium);
router.patch('/make-user-basic', Billing.makeUserBasic);
router.patch('/cancel-subscription', Billing.cancelSubscription);

export default router;
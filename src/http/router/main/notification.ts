import {Router} from "express";
import * as Notifications from "../../controller/notification";

const router = Router();

router.get('/', Notifications.listAction);
router.patch('/text-notifications/:id/read', Notifications.readTextNotification);

export default router;
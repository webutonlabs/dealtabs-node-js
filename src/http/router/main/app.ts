import {Router} from "express";
import * as App from "../../controller/app";

const router = Router();

router.use('/memberships', App.membershipsViewAction);
router.use('/blocking-version', App.blockingVersion);

export default router;
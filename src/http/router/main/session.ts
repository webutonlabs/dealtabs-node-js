import {Router} from "express";
import * as Session from "../../controller/session";

const router = Router();

router.param('id', Session.ParamHandler.id);
router.param('referenceId', Session.ParamHandler.referenceId);

router.post('/', Session.createAction);
router.get('/:id', Session.viewAction);
router.delete('/:id', Session.deleteAction);
router.get('/', Session.listAction);
router.patch('/:id', Session.editAction);
router.put('/order', Session.orderAction);
router.post('/:id/share', Session.createSharedAction);
router.patch('/:id/order-windows', Session.orderWindowsAction);
router.get('/shared/:referenceId', Session.viewBySharedReferenceId);
router.post('/shared/:referenceId/save', Session.saveSharedSession);

export default router;
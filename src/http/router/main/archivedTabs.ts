import {Router} from "express";
import * as ArchivedTabs from "../../controller/archivedTabs";

const router = Router();
router.param('id', ArchivedTabs.ParamHandler.id);

router.get('/', ArchivedTabs.listAction);
router.delete('/:id', ArchivedTabs.deleteAction);

export default router;
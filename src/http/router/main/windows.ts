import {Router} from "express";
import * as SessionWindow from "../../controller/windows";

const router = Router();

router.param('id', SessionWindow.ParamHandler.id);

router.post('/', SessionWindow.createMultipleAction);
router.delete('/:id', SessionWindow.deleteAction);
router.patch('/:id', SessionWindow.editAction);
router.patch('/:id/order-tabs', SessionWindow.orderTabsAction);
router.patch('/:id/replace-tabs', SessionWindow.replaceTabsAction);

export default router;
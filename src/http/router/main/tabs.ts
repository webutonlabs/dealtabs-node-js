import {Router} from "express";
import * as WindowTab from "../../controller/tabs";

const router = Router();

router.param('id', WindowTab.ParamHandler.id);

router.post('/', WindowTab.createMultipleAction);
router.delete('/:id', WindowTab.deleteAction);
router.patch('/:id', WindowTab.editAction);

export default router;
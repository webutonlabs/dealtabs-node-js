import express from 'express';
import session from './session';
import sessionWindow from './windows';
import windowTab from './tabs';
import app from './app';
import user from './user';
import archivedTabs from "./archivedTabs";
import liveSync from "./liveSync";
import quickTabs from "./quickTabs";
import collaboration from "./collaboration";
import notifications from './notification';

const router = express.Router();

router.use('/parameters', app);
router.use('/sessions', session);
router.use('/session-windows', sessionWindow);
router.use('/window-tabs', windowTab);
router.use('/user', user);
router.use('/archived-tabs', archivedTabs);
router.use('/live-sync', liveSync);
router.use('/quick-tabs', quickTabs);
router.use('/collaboration', collaboration);
router.use('/notifications', notifications);

export default router;
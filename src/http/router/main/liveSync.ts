import {Router} from "express";
import * as LiveSync from "../../controller/liveSync";

const router = Router();

router.get('/', LiveSync.listSyncedDevices);
router.patch('/edit-device', LiveSync.editSyncedDevice);
router.delete('/delete-device', LiveSync.deleteSyncedDevice);
router.put('/replace-synced-tabs', LiveSync.replaceSyncedDeviceTabs);
router.get('/view-device', LiveSync.viewSyncedDevice);

export default router;
import {Router} from "express";
import * as QuickTab from "../../controller/quickTab";

const router = Router();
router.param('id', QuickTab.ParamHandler.id);

router.get('/', QuickTab.listAction);
router.post('/', QuickTab.createMultiple);
router.delete('/:id', QuickTab.deleteAction);
router.patch('/:id', QuickTab.editAction);
router.put('/', QuickTab.sortAction);

export default router;
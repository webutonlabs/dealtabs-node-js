import {Router} from "express";
import * as User from "../../controller/user";

const router = Router();

router.delete('/', User.deleteAction);
router.patch('/', User.editAction);
router.get('/', User.viewAction);
router.patch('/cancel-subscription', User.cancelSubscription);

export default router;
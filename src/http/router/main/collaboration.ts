import {Router} from "express";
import * as Collaboration from "../../controller/collaboration";

const router = Router();

router.post('/confirm-invitation', Collaboration.confirmCollaboratedSession);
router.post('/invite', Collaboration.inviteCollaboratorAction);
router.get('/', Collaboration.listCollaborators);
router.patch('/delete-collaborator', Collaboration.deleteCollaboratorAction);
router.patch('/', Collaboration.toggleSessionCollaboration);

export default router;
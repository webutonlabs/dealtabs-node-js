import {Router} from "express";
import * as Auth from "../controller/auth";

const router = Router();

router.post('/', Auth.authAction);
router.post('/registration/by-username', Auth.registerByUsernameAction);
router.post('/registration/confirm-email', Auth.confirmEmail);
router.post('/registration/resend-email', Auth.resendEmail);
router.post('/password-recovery', Auth.requestPasswordRecovery);
router.post('/password-recovery/change-password', Auth.changePassword);

export default router;
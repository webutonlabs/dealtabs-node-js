import {NextFunction, Request, RequestHandler, Response} from "express";

export const unless = (excludePaths: string[], middleware: any): RequestHandler => {
    return (req: Request, res: Response, next: NextFunction) => {
        for (const excludePath of excludePaths) {
            if (req.path.startsWith(excludePath)) {
                return next();
            }
        }

        return middleware(req, res, next);
    }
}
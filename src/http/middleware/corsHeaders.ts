import {ResolvedRequest} from "../request/ResolvedRequest";
import {NextFunction, Response} from "express";

export const corsHeaders = async (req: ResolvedRequest, res: Response, next: NextFunction) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Expose-Headers', '*');

    next();
}
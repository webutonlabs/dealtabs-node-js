import {NextFunction, Response} from "express";
import AuthError from "../../response/exception/AuthError";
import {ResolvedRequest} from "../../request/ResolvedRequest";

export const billingAuthenticator = (req: ResolvedRequest, res: Response, next: NextFunction) => {
    const accessToken = process.env.BILLING_ACCESS_TOKEN;
    const requestToken = req.header('X-BILLING-TOKEN');

    if (accessToken !== requestToken) {
        return next(new AuthError('Token is invalid'));
    }

    return next();
}
import {getConnection} from "typeorm";
import {AccessToken} from "../../../domain/entity/user/AccessToken";
import AuthError from "../../response/exception/AuthError";
import {ResolvedRequest} from "../../request/ResolvedRequest";
import {NextFunction, Response} from "express";
import {User} from "../../../domain/entity/user/User";

export const tokenAuthenticator = async (req: ResolvedRequest, res: Response, next: NextFunction) => {
    const accessToken = await getConnection().getRepository(AccessToken).findOne(
        {
            where: {
                token: req.header('X-AUTH-TOKEN'),
            },
            relations: ['user', 'user.membership']
        },
    );

    if (accessToken === undefined) {
        return next(new AuthError('Access token was not found'));
    }

    if (new Date(accessToken.expiresAt).getTime() < new Date().getTime()) {
        return next(new AuthError('Access token is expired'));
    }

    const user: User = accessToken.user;

    req.user = user;
    if (false === req.user.isEmailConfirmed) {
        return next(new AuthError('You must confirm your email address'));
    }

    return next();
};

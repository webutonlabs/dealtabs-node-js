import {ResolvedRequest} from "../../request/ResolvedRequest";
import {NextFunction, Response} from "express";
import AuthError from "../../response/exception/AuthError";

export const cronAuthenticator = (req: ResolvedRequest, res: Response, next: NextFunction) => {
    const accessToken = process.env.CRON_ACCESS_TOKEN;
    const requestToken = req.header('X-CRON-TOKEN');

    if (accessToken !== requestToken) {
        return next(new AuthError('Token is invalid'));
    }

    return next();
}
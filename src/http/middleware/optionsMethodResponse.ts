import {ResolvedRequest} from "../request/ResolvedRequest";
import {NextFunction, Response} from "express";

export const optionsMethodResponse = async (req: ResolvedRequest, res: Response, next: NextFunction) => {
    if (req.method === 'OPTIONS') {
        res.status(200).json({});
    } else {
        next();
    }
}
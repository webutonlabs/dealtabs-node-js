import {ResolvedRequest} from "../request/ResolvedRequest";
import {NextFunction, Response} from "express";

export const resolveRequest = (req: ResolvedRequest, res: Response, next: NextFunction) => {
    req.deviceId = req.header('X-DEVICE-ID');
    req.userAgent = req.header('User-Agent');
    req.resolvedParams = {};

    return next();
}
import { ErrorRequestHandler } from 'express';
import AuthError from "../response/exception/AuthError";
import NotFound from "../response/exception/NotFound";
import BadRequest from "../response/exception/BadRequest";
import {error} from "../../infrustructure/service/logs/logs";
import {ResolvedRequest} from "../request/ResolvedRequest";

export const errorHandler: ErrorRequestHandler = (err, req: ResolvedRequest, res, next) => {
    switch (true) {
        case err instanceof AuthError:
            return res.status(403).json({'message': err.message});

        case err instanceof NotFound:
            return res.status(404).json({'message': err.message});

        case err instanceof BadRequest:
            return res.status(400).json({'message': err.message});

        default:
            error(err.message, {stack: err.stack});

            return res.status(500).json({'message': err.message});
    }
};
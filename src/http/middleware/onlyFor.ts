import {NextFunction, Request, RequestHandler, Response} from "express";

export const onlyFor = (paths: string[], middleware: any): RequestHandler => {
    return (req: Request, res: Response, next: NextFunction) => {
        for (const path of paths) {
            if (req.path.startsWith(path)) {
                return middleware(req, res, next);
            }
        }

        return next();
    }
}
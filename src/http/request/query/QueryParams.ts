export class QueryParams {
    private readonly _query: object;

    constructor(query: object) {
        this._query = query;
    }

    public has(key: string): boolean {
        return this._query.hasOwnProperty(key);
    }

    public get(key: string): any {
        if (!this.has(key)) {
            return undefined;
        }

        // @ts-ignore
        return this._query[key];
    }

    getBoolean(key: string): boolean {
        return this.get(key) === 'true';
    }

    getMultiple(key: string): string[] {
        return this.get(key) === undefined ? undefined : this.get(key).split(',');
    }
}
import {Request} from "express";
import {User} from "../../domain/entity/user/User";

interface ResolvedParams {
    [key: string]: any
}

export interface ResolvedRequest extends Request {
    user: User | undefined,
    deviceId: string | undefined,
    userAgent: string | undefined,
    resolvedParams: ResolvedParams
}
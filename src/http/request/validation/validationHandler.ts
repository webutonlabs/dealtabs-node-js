import {RequestHandler} from "express";
import {validationResult} from "express-validator";

export const validationHandler = () : RequestHandler => {
    return (req: any, res: any, next: any) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            let responseBody: object[] = [];

            errors.array().forEach((error) => {
                responseBody.push({message: error.msg, pointer: error.param});
            })

            return res.status(400).json({ errors: responseBody });
        }

        return next();
    }
}
import {getConnection} from "typeorm";
import {User} from "../../../../domain/entity/user/User";

export const failIfUserExistByEmail = async (email: string) => {
    const user = await getConnection().getRepository(User).findOne({where: {email: email.toLowerCase()}});

    if (undefined !== user) {
        throw new Error('User already exists');
    }

    return true;
}
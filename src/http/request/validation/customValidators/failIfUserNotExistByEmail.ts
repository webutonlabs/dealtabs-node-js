import {getConnection} from "typeorm";
import {User} from "../../../../domain/entity/user/User";

export const failIfUserNotExistByEmail = async (email: string) => {
    const user = await getConnection().getRepository(User).findOne({where: {email: email}});

    if (undefined === user) {
        throw new Error('User does not exists');
    }

    return true;
}
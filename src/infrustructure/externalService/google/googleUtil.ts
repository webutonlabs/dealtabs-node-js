import { google } from 'googleapis';
import { googleConfig } from "../../../config/google";

const createAuth = () => {
    return new google.auth.OAuth2(
        googleConfig.clientId,
        googleConfig.clientSecret,
        googleConfig.redirectUrl
    );
}

export { createAuth }
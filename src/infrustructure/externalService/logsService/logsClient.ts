import axios from "axios";

const logsClient = () => {
    return axios.create({
        baseURL: process.env.LOGS_SERVICE_HOST,
        headers: {
            'Content-Type': 'application/json',
            'X-AUTH-TOKEN': process.env.LOGS_SERVICE_ACCESS_TOKEN,
            'X-SERVICE': 'dealtabs_api',
            Accept: 'application/json',
        },
    });
}

export {
    logsClient
}
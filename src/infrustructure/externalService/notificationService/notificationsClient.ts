import axios from "axios";

const notificationsClient = () => {
    return axios.create({
        baseURL: process.env.NOTIFICATION_SERVICE_HOST + `/dealtabs`,
        headers: {
            'Content-Type': 'application/json',
            'X-AUTH-TOKEN': process.env.NOTIFICATION_SERVICE_ACCESS_TOKEN,
            Accept: 'application/json',
        },
    });
}

export {
    notificationsClient
}
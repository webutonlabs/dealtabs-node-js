import axios from "axios";

const notificationsEmailClient = (locale: string) => {
    return axios.create({
        baseURL: process.env.NOTIFICATION_SERVICE_HOST + `/${locale}/dealtabs/emails`,
        headers: {
            'Content-Type': 'application/json',
            'X-AUTH-TOKEN': process.env.NOTIFICATION_SERVICE_ACCESS_TOKEN,
            Accept: 'application/json',
        },
    });
}

export {
    notificationsEmailClient
}
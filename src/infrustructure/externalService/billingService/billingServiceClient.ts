import axios from "axios";

const billingServiceClient = (locale: string) => {
    return axios.create({
        baseURL: process.env.BILLING_SERVICE_HOST + `/${locale}/dealtabs`,
        headers: {
            'Content-Type': 'application/json',
            'X-AUTH-TOKEN': process.env.BILLING_SERVICE_ACCESS_TOKEN,
            Accept: 'application/json',
        },
    });
}

export {
    billingServiceClient
}
import {User} from "../domain/entity/user/User";

const filterByIsEnabledStatus = (entities: any) => {
    let enabled = [], disabled = [];

    for (const entity of entities) {
        entity.isEnabled ? enabled.push(entity) : disabled.push(entity);
    }

    return [enabled, disabled];
}

const randomCode = (length: number = 9) => {
    const randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';

    for (let i = 0; i < length; i++) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }

    return result;
}

const isUserPremium = (user: User) => {
    return null === user.membership.subscriptionExpires
        ? false
        : (new Date(user.membership.subscriptionExpires).getTime() > new Date().getTime());
}

export {
    filterByIsEnabledStatus,
    randomCode,
    isUserPremium
}
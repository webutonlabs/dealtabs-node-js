import {membershipTypesCollection, Membership} from "../../config/membership";
import {User} from "../../domain/entity/user/User";
import {MembershipTypes} from "../../domain/entity/user/Membership";

export class MembershipRestrictions {
    private readonly _data: Membership[];
    private _membershipData: Membership

    constructor() {
        this._data = membershipTypesCollection;
    }

    public forUser(user: User) {
        switch (user.membership.membershipType) {
            case MembershipTypes.basic: {
                this.forBasic();

                break;
            }
            case MembershipTypes.premium: {
                this.forPremium();

                break;
            }
        }

        return this;
    }

    public forBasic() {
        this._setMembershipData(MembershipTypes.basic);

        return this;
    }

    public forPremium() {
        this._setMembershipData(MembershipTypes.premium);

        return this;
    }

    public sessionLimit(): number | boolean {
        return this._membershipData.restrictions.sessionLimit;
    }

    public windowsLimit(): number | boolean {
        return this._membershipData.restrictions.windowsLimit;
    }

    public tabsLimit(): number | boolean {
        return this._membershipData.restrictions.tabsLimit;
    }

    public quickTabsLimit(): number | boolean {
        return this._membershipData.restrictions.quickTabsLimit;
    }

    public deviceSync(): boolean {
        return this._membershipData.restrictions.deviceSync;
    }

    public liveSync(): boolean {
        return this._membershipData.restrictions.liveSync;
    }

    public collaboration(): boolean {
        return this._membershipData.restrictions.collaboration;
    }

    public saveSharedSession(): boolean {
        return this._membershipData.restrictions.saveSharedSession;
    }

    private _setMembershipData(membershipType: string) {
        this._membershipData = this._data.find(membership => membership.type === membershipType);
    }
}
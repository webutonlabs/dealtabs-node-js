import {getConnection} from "typeorm";
import {CollaboratorRepository} from "../../domain/repository/CollaboratorRepository";
import {CollaboratedSessionRepository} from "../../domain/repository/CollaboratedSessionRepository";
import AuthError from "../../http/response/exception/AuthError";

export const checkCollaboratorSessionAccess = async (userEmail: string, sessionId: number) => {
    const collaborator = await getConnection()
        .getCustomRepository(CollaboratorRepository).findByEmailOrThrow(userEmail);

    let collaboratedSession = undefined;
    try {
         collaboratedSession = await getConnection()
            .getCustomRepository(CollaboratedSessionRepository)
            .findByCollaboratorAndSessionOrThrow(collaborator, sessionId);
    } catch (e) {
        throw new AuthError('Access denied.');
    }

    if (!collaboratedSession.isConfirmed) {
        throw new AuthError('Access denied.');
    }
}
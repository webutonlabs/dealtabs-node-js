import {User} from "../../../domain/entity/user/User";
import {Membership, MembershipTypes, SubscriptionTypes} from "../../../domain/entity/user/Membership";
import {getConnection} from "typeorm";
import {notificationsEmailClient} from "../../externalService/notificationService/notificationsEmailClient";
import {error} from "../logs/logs";

const registerFromSocials = async (name: string, email: string, locale:string, surname?: string) => {
    const user = new User();
    user.name = name;
    user.email = email;
    user.locale = locale;
    user.surname = surname;
    user.isEmailConfirmed = true;

    const currentDate = new Date();
    user.membership = new Membership(
        MembershipTypes.premium,
        SubscriptionTypes.gift,
        new Date(currentDate.setMonth(currentDate.getMonth() + 6))
    );

    await getConnection().getRepository(User).save(user);

    notificationsEmailClient(user.locale).post('/welcome-message', {
        user: {
            name: user.name,
            email: user.email
        }
    }).then((res) => {
        //
    }).catch(err => {
        error(err.message, {user_id: user.id, stack: err.stack});
    });

    return user;
}

export {registerFromSocials}
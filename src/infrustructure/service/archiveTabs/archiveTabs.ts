import {WindowTab} from "../../../domain/entity/session/WindowTab";
import {ArchivedTab} from "../../../domain/entity/archive/ArchivedTab";
import {User} from "../../../domain/entity/user/User";
import {getConnection} from "typeorm";
import {QuickTab} from "../../../domain/entity/QuickTab/QuickTab";

const archiveTabsForUser = async (tabs: WindowTab[]|QuickTab[], user: User) => {
    let archivedTabs = [];

    for (const tab of tabs) {
        archivedTabs.push(
            new ArchivedTab(
                user,
                tab.name,
                tab.url,
                tab.icon
            )
        );
    }

    await getConnection().getRepository(ArchivedTab).save(archivedTabs);
}

export {archiveTabsForUser}
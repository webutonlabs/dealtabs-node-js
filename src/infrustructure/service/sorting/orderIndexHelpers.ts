import {User} from "../../../domain/entity/user/User";
import {Session} from "../../../domain/entity/session/Session";
import {getConnection} from "typeorm";
import {SessionRepository} from "../../../domain/repository/SessionRepository";
import {CollaboratedSessionRepository} from "../../../domain/repository/CollaboratedSessionRepository";
import {CollaboratedSession} from "../../../domain/entity/collaboration/CollaboratedSession";

const decrementIndexesOfOtherSessions = async (user: User, comparisonIndex = 0) => {
    const sessions: Session[] = await getConnection()
        .getCustomRepository(SessionRepository).findByUser(user.id)
    const collaboratedSessions = await getConnection()
        .getCustomRepository(CollaboratedSessionRepository).listByUser(user);

    for (const session of [...sessions, ...collaboratedSessions]) {
        if (session.orderIndex > comparisonIndex) {
            --session.orderIndex;
        }
    }

    await getConnection().transaction(async em => {
        await em.getRepository(Session).save(sessions);
        await em.getRepository(CollaboratedSession).save(collaboratedSessions);
    })
}

export {
    decrementIndexesOfOtherSessions
}
import {logsClient} from "../../externalService/logsService/logsClient";

const info = (message: string, data: object = {}) => {
    logsClient().post('/logs', {type: 'info', main_message: message, data: data})
        .then(() => {})
        .catch((err) => {console.log(err)})
}

const warning = (message: string, data: object = {}) => {
    logsClient().post('/logs', {type: 'warning', main_message: message, data: data})
        .then(() => {})
        .catch((err) => {console.log(err)})
}

const error = (message: string, data: object = {}) => {
    logsClient().post('/logs', {type: 'error', main_message: message, data: data})
        .then(() => {})
        .catch((err) => {console.log(err)})
}

export {
    info,
    warning,
    error
}
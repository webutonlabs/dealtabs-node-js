const path = require('path');
const fs = require("fs");

const translator = (locale: string, defaultLocale = 'en') => {
    const translationsDir = path.join(__dirname, '../../../translations');
    let translationPath = translationsDir + `/${locale}.json`;

    if (!fs.existsSync(translationPath)) {
        translationPath = translationsDir + `/${defaultLocale}.json`;
    }

    const file = fs.readFileSync(translationPath, 'utf8');
    const translations = JSON.parse(file);

    return new Trans(translations);
}

class Trans {
    private readonly _translations:any;

    constructor(translations:any) {
        this._translations = translations;
    }

    trans = (key:string) => {
        return this._translations[key];
    }
}

export {translator};
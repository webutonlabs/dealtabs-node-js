import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "../user/User";

@Entity("archived_tabs")
export class ArchivedTab {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    url: string;

    @Column({default: null})
    icon?: string;

    @Column({type: "timestamp"})
    creationDate: Date

    @ManyToOne(type => User, {onDelete: 'CASCADE'})
    user: User;

    constructor(
        user: User,
        name: string,
        url: string,
        icon?: string,
    ) {
        this.user = user;
        this.name = name;
        this.url = url;
        this.icon = icon;
        this.creationDate = new Date();
    }
}
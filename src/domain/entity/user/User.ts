import {Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn, Unique} from 'typeorm';
import {Membership} from "./Membership";
import {Session} from "../session/Session";
import {AccessToken} from "./AccessToken";
import {QuickTab} from "../QuickTab/QuickTab";
import {Collaborator} from "../collaboration/Collaborator";

@Entity("users")
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({default: null})
    surname?: string;

    @Column({default: null})
    password?: string;

    @Column({unique: true})
    email: string;

    @Column()
    isEmailConfirmed: boolean;

    @Column({nullable: true})
    locale?: string;

    @OneToOne(type => Membership, {onDelete: "CASCADE", nullable: false, cascade: true})
    @JoinColumn()
    membership: Membership;

    @OneToMany(type => Session, session => session.user, {cascade: true})
    sessions: Session[];

    @OneToMany(type => Collaborator, collaborator => collaborator.user, {cascade: true})
    collaborator: Collaborator[];

    @OneToMany(type => QuickTab, quickTab => quickTab.user, {cascade: true})
    quickTabs: QuickTab[];

    @OneToMany(type => AccessToken, accessToken => accessToken.user, {cascade: true})
    accessTokens: AccessToken[]

    @Column({default: true})
    emailSubscription: boolean;

    constructor() {
        this.locale = 'en';
        this.emailSubscription = true;
    }
}
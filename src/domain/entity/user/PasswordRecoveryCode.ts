import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {User} from "./User";

@Entity()
export class PasswordRecoveryCode {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @ManyToOne(type => User, {onDelete: 'CASCADE'})
    user: User;

    constructor(code: string, user: User) {
        this.code = code;
        this.user = user;
    }
}
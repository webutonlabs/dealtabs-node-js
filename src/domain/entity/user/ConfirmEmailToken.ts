import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {User} from "./User";

@Entity()
export class ConfirmEmailToken {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    token: string;

    @ManyToOne(type => User, {onDelete: 'CASCADE'})
    user: User;

    constructor(token: string, user: User) {
        this.token = token;
        this.user = user;
    }
}
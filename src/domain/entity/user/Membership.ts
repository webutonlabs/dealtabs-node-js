import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

export enum SubscriptionTypes {
    none = 'none',
    gift = 'gift',
    expiring = 'expiring',
    premium_dev = 'premium_dev',
    liqpay_one_time_month = 'liqpay_one_time_month',
    liqpay_one_time_year = 'liqpay_one_time_month',
    liqpay_recurring_monthly = 'liqpay_recurring_monthly',
    liqpay_recurring_yearly = 'liqpay_recurring_yearly',
}

export const enum MembershipTypes {
    basic = 'basic',
    premium = 'premium'
}

@Entity()
export class Membership {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({default: MembershipTypes.basic})
    membershipType: string;

    @Column({type: "timestamp", default: null})
    subscriptionExpires?: Date

    @Column({nullable: true})
    subscriptionType: string;

    @Column()
    allowedTrial: boolean;

    @Column()
    cancelledSubscription: boolean;

    constructor(
        membershipType: MembershipTypes = MembershipTypes.basic,
        subscriptionType: SubscriptionTypes = SubscriptionTypes.none,
        subscriptionExpires: Date = null,
        allowedTrial: boolean = true,
        cancelledSubscription: boolean = false
    ) {
        this.membershipType = membershipType;
        this.subscriptionType = subscriptionType;
        this.subscriptionExpires = subscriptionExpires;
        this.allowedTrial = allowedTrial;
        this.cancelledSubscription = cancelledSubscription;
    }
}
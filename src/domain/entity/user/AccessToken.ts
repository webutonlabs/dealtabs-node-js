import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {User} from "./User";

@Entity()
export class AccessToken {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    token: string;

    @Column({type: "timestamp"})
    expiresAt: Date

    @ManyToOne(type => User, {onDelete: 'CASCADE'})
    user: User;

    constructor(token: string, expiresAt: Date, user: User) {
        this.token = token;
        this.expiresAt = expiresAt;
        this.user = user;
    }
}
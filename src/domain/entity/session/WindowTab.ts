import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {SessionWindow} from "./SessionWindow";

@Entity()
export class WindowTab {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    url: string;

    @Column({default: null})
    icon?: string;

    @Column({type: "timestamp"})
    creationDate: Date

    @Column()
    orderIndex: number;

    @Column()
    isEnabled: boolean;

    @ManyToOne(type => SessionWindow, {onDelete: "CASCADE"})
    window: SessionWindow;

    constructor(
        name: string,
        url: string,
        window: SessionWindow,
        icon?: string,
        isEnabled: boolean = true
    ) {
        this.name = name;
        this.url = url;
        this.icon = icon;
        this.window = window;
        this.isEnabled = isEnabled;
        this.creationDate = new Date();
        this.orderIndex = 0;
    }
}
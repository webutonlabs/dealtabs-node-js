import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Session} from "./Session";
import {WindowTab} from "./WindowTab";


@Entity()
export class SessionWindow {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({default: null})
    name?: string;

    @ManyToOne(type => Session, {onDelete: "CASCADE"})
    session: Session;

    @Column({type: "timestamp"})
    creationDate: Date

    @Column()
    orderIndex: number;

    @Column()
    isEnabled: boolean;

    @OneToMany(type => WindowTab, windowTab => windowTab.window, {cascade: true})
    tabs: WindowTab[];

    constructor(session: Session, name?: string, isEnabled: boolean = true) {
        this.session = session;
        this.name = name;
        this.isEnabled = isEnabled;
        this.orderIndex = 0;
        this.creationDate = new Date();
    }
}
import {Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Session} from "./Session";

@Entity()
export class SharedSession {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(() => Session, {onDelete: "CASCADE"})
    @JoinColumn()
    session: Session;

    @Column("uuid")
    referenceId: string;

    constructor(session: Session, referenceId: string) {
        this.session = session;
        this.referenceId = referenceId;
    }
}
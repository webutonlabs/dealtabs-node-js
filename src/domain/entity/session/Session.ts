import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {User} from "../user/User";
import {SessionWindow} from "./SessionWindow";

@Entity("sessions")
export class Session {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(type => User, {onDelete: 'CASCADE'})
    user: User;

    @Column({type: "timestamp"})
    creationDate: Date

    @Column()
    orderIndex: number;

    @Column()
    isEnabled: boolean;

    @Column()
    deviceId: string;

    @Column()
    userAgent: string;

    @OneToMany(type => SessionWindow, sessionWindow => sessionWindow.session, {cascade: true})
    windows: SessionWindow[];

    @Column({default: false})
    enabledForCollaboration: boolean;

    // meta data
    [key: string]: any

    constructor(
        name: string,
        userAgent: string,
        user: User,
        deviceId: string,
        isEnabled: boolean = true
    ) {
        this.name = name;
        this.userAgent = userAgent;
        this.user = user;
        this.deviceId = deviceId;
        this.isEnabled = isEnabled;
        this.creationDate = new Date();
        this.orderIndex = 0;
        this.enabledForCollaboration = false;
    }
}
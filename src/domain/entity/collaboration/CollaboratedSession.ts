import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "../user/User";
import {Collaborator} from "./Collaborator";
import {Session} from "../session/Session";

@Entity()
export class CollaboratedSession {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Session, {onDelete: 'CASCADE'})
    relatedSession: Session;

    @ManyToOne(type => Collaborator, {onDelete: 'CASCADE'})
    collaborator: Collaborator;

    @ManyToOne(type => User, {onDelete: 'CASCADE'})
    owner: User

    @Column()
    orderIndex: number;

    @Column()
    isEnabled: boolean;

    @Column({type: "timestamp"})
    creationDate: Date

    @Column()
    isActive: boolean;

    @Column({default: false})
    isConfirmed: boolean;

    constructor(
        relatedSession: Session,
        collaborator: Collaborator,
        owner: User
    ) {
        this.relatedSession = relatedSession;
        this.collaborator = collaborator;
        this.owner = owner;
        this.isEnabled = true;
        this.creationDate = new Date();
        this.orderIndex = 0;
        this.isActive = true;
        this.isConfirmed = false;
    }
}
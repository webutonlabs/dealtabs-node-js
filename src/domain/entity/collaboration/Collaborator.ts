import {Column, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "../user/User";
import {CollaboratedSession} from "./CollaboratedSession";

@Entity()
export class Collaborator {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: "timestamp"})
    creationDate: Date

    @ManyToOne(() => User, {onDelete: 'CASCADE', nullable: true})
    user: User;

    @Column({unique: true, nullable: true})
    email?: string;

    @OneToMany(type => CollaboratedSession, session => session.collaborator, {cascade: true})
    collaboratedSessions: CollaboratedSession[]

    constructor(
        user?: User,
        email?: string
    ) {
        this.user = user;
        this.email = email;
        this.creationDate = new Date();
    }
}
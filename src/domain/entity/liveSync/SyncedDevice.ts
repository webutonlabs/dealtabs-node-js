import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {User} from "../user/User";
import {SyncedTab} from "./SyncedTab";

@Entity("live_sync_devices")
export class SyncedDevice {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    deviceId: string;

    @Column()
    isSyncEnabled: boolean;

    @ManyToOne(type => User, {onDelete: 'CASCADE'})
    user: User;

    @OneToMany(type => SyncedTab, syncedTab => syncedTab.syncedDevice, {cascade: true})
    syncedTabs: SyncedTab[];

    constructor(
        user: User,
        name: string,
        deviceId: string,
    ) {
        this.user = user;
        this.name = name;
        this.deviceId = deviceId;
        this.isSyncEnabled = true;
    }
}
import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {SyncedDevice} from "./SyncedDevice";

@Entity("live_sync_tabs")
export class SyncedTab {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    url: string;

    @Column({default: null})
    icon?: string;

    @ManyToOne(type => SyncedDevice, {onDelete: 'CASCADE'})
    syncedDevice: SyncedDevice;

    @Column()
    orderIndex: number;

    @Column()
    isActive: boolean;

    constructor(
        syncedDevice: SyncedDevice,
        name: string,
        url: string,
        orderIndex: number,
        isActive: boolean,
        icon?: string,
    ) {
        this.syncedDevice = syncedDevice;
        this.name = name;
        this.url = url;
        this.icon = icon;
        this.orderIndex = orderIndex;
        this.isActive = isActive;
    }
}
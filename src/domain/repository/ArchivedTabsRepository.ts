import {EntityRepository, getConnection, Repository} from "typeorm";
import {ArchivedTab} from "../entity/archive/ArchivedTab";
import {User} from "../entity/user/User";

@EntityRepository(ArchivedTab)
export class ArchivedTabsRepository extends Repository<ArchivedTab> {
    async listForUser(user: User) {
        const qb = await getConnection()
            .createQueryBuilder(ArchivedTab, 't')
            .leftJoin('t.user', 'tu')
            .where('tu.id = :userId', {userId: user.id})
            .orderBy('t.creationDate', 'DESC');

        return qb.paginate();
    }
}
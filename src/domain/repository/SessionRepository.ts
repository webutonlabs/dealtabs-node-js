import {EntityRepository, getConnection, Repository} from "typeorm";
import {Session} from "../entity/session/Session";
import {User} from "../entity/user/User";

@EntityRepository(Session)
export class SessionRepository extends Repository<Session> {
    async listUserSessions(
        user: User,
        deviceSync: boolean,
        deviceId: string = null,
        relations: string[] = []
    ) {
        const qb = await getConnection()
            .createQueryBuilder(Session, 's')
            .leftJoin('s.user', 'su')
            .where('su.id = :userId', {userId: user.id})
            .orderBy('s.orderIndex', 'ASC');

        if (relations.includes('windows')) {
            qb.leftJoinAndSelect('s.windows', 'sw');
        }

        if (relations.includes('tabs')) {
            qb.leftJoinAndSelect('sw.tabs', 'swt');
        }

        if (deviceSync === false) {
            qb.andWhere('s.deviceId = :deviceId', {deviceId: deviceId})
        }

        return qb.getMany();
    }

    async findByUserAndDevice(user: User, deviceId: string, offset: number = 0, isEnabled: boolean = null) {
        const qb = await getConnection()
            .createQueryBuilder(Session, 's')
            .leftJoin('s.user', 'su')
            .where('s.deviceId = :deviceId', {deviceId: deviceId})
            .andWhere('su.id = :userId', {userId: user.id})
            .offset(offset)
            .orderBy('s.orderIndex', 'ASC');

        if (null !== isEnabled) {
            qb.andWhere('s.isEnabled = :isEnabled', {isEnabled: isEnabled});
        }

        return qb.getMany();
    };

    async findByUserAndOrderIndex(userId: number, orderIndex: number, comparison: string = '>') {
        return await getConnection()
            .createQueryBuilder(Session, 's')
            .where(`s.orderIndex ${comparison} :orderIndex`, {orderIndex: orderIndex})
            .andWhere('s.user = :userId', {userId: userId})
            .getMany();
    }

    async findById(id: number) {
        return await getConnection()
            .createQueryBuilder(Session, 's')
            .where('s.id = :id', {id: id})
            .leftJoinAndSelect('s.windows', 'sw')
            .leftJoinAndSelect('s.user', 'su')
            .addOrderBy('sw.orderIndex', 'ASC')
            .getOne();
    }

    async findByUser(userId: number) {
        return await getConnection()
            .createQueryBuilder(Session, 's')
            .leftJoinAndSelect('s.user', 'su')
            .where('su.id = :id', {id: userId})
            .addOrderBy('s.orderIndex', 'ASC')
            .getMany();
    }

}
import {EntityRepository, getConnection, Repository} from "typeorm";
import {WindowTab} from "../entity/session/WindowTab";

@EntityRepository(WindowTab)
export class WindowTabRepository extends Repository<WindowTab> {
    async findByWindowAndOrderIndex(windowId: number, orderIndex: number, comparison: string = '>') {
        return await getConnection()
            .createQueryBuilder(WindowTab, 'wt')
            .where('wt.window = :windowId', {windowId: windowId})
            .andWhere(`wt.orderIndex ${comparison} :orderIndex`, {orderIndex: orderIndex})
            .orderBy('wt.orderIndex', 'ASC')
            .getMany();
    }
}
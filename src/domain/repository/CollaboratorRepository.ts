import {EntityRepository, getConnection, Repository} from "typeorm";
import {Collaborator} from "../entity/collaboration/Collaborator";
import NotFound from "../../http/response/exception/NotFound";

@EntityRepository(Collaborator)
export class CollaboratorRepository extends Repository<Collaborator> {
    async findByEmailOrThrow(email: string) {
        const qb = await getConnection()
            .createQueryBuilder(Collaborator, 'c')
            .leftJoinAndSelect('c.user', 'cu')
            .where('c.email = :email', {email: email})
            .orWhere('cu.email = :email', {email: email});

        const collaborator = qb.getOne();

        if (undefined === collaborator) {
            throw new NotFound('Collaborator was not found');
        }

        return collaborator;
    }

    async findByEmail(email: string) {
        const qb = await getConnection()
            .createQueryBuilder(Collaborator, 'c')
            .leftJoinAndSelect('c.user', 'cu')
            .where('c.email = :email', {email: email})
            .orWhere('cu.email = :email', {email: email});

        return qb.getOne();
    }
}
import {EntityRepository, getConnection, Repository} from "typeorm";
import {QuickTab} from "../entity/QuickTab/QuickTab";
import {User} from "../entity/user/User";

@EntityRepository(QuickTab)
export class QuickTabRepository extends Repository<QuickTab> {
    async findByUserAndOrderIndex(userId: number, orderIndex: number, comparison: string = '>') {
        return await getConnection()
            .createQueryBuilder(QuickTab, 'qt')
            .where('qt.user = :userId', {userId: userId})
            .andWhere(`qt.orderIndex ${comparison} :orderIndex`, {orderIndex: orderIndex})
            .orderBy('qt.orderIndex', 'ASC')
            .getMany();
    }

    async listForUser(user: User) {
        const qb = await getConnection()
            .createQueryBuilder(QuickTab, 't')
            .leftJoin('t.user', 'tu')
            .where('tu.id = :userId', {userId: user.id})
            .orderBy('t.orderIndex', 'ASC');

        return qb.paginate();
    }
}
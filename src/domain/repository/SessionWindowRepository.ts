import {EntityRepository, getConnection, Repository} from "typeorm";
import {SessionWindow} from "../entity/session/SessionWindow";

@EntityRepository(SessionWindow)
export class SessionWindowRepository extends Repository<SessionWindow> {
    async findBySession(sessionId: number) {
        return await getConnection()
            .createQueryBuilder(SessionWindow, 'sw')
            .where('sw.session = :sessionId', {sessionId: sessionId})
            .orderBy('sw.orderIndex', 'ASC')
            .getMany();
    }

    async findBySessionAndOrderIndex(sessionId: number, orderIndex: number, comparison: string = '>') {
        return await getConnection()
            .createQueryBuilder(SessionWindow, 'sw')
            .where('sw.session = :sessionId', {sessionId: sessionId})
            .andWhere(`sw.orderIndex ${comparison} :orderIndex`, {orderIndex: orderIndex})
            .orderBy('sw.orderIndex', 'ASC')
            .getMany();
    }

    async findById(id: number) {
        return await getConnection()
            .createQueryBuilder(SessionWindow, 'sw')
            .where('sw.id = :id', {id: id})
            .leftJoinAndSelect('sw.tabs', 'wt')
            .leftJoinAndSelect('sw.session', 's')
            .leftJoinAndSelect('s.user', 'su')
            .addOrderBy('sw.orderIndex', 'ASC')
            .addOrderBy('wt.orderIndex', 'ASC')
            .getOne();
    }
}
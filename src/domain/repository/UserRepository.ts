import {EntityRepository, getConnection, Repository} from "typeorm";
import {User} from "../entity/user/User";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    async findUsers(offset: number, limit: number) {
        return await getConnection()
            .createQueryBuilder(User, 'u')
            .leftJoinAndSelect('u.membership', 'm')
            .leftJoinAndSelect('u.sessions', 's')
            .leftJoinAndSelect('s.windows', 'w')
            .leftJoinAndSelect('w.tabs', 't')
            .offset(offset)
            .addOrderBy('u.id', 'ASC')
            .addOrderBy('s.orderIndex', 'ASC')
            .addOrderBy('w.orderIndex', 'ASC')
            .addOrderBy('t.orderIndex', 'ASC')
            .limit(limit)
            .getMany();
    }
}
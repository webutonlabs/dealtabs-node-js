import {EntityRepository, getConnection, Repository} from "typeorm";
import {CollaboratedSession} from "../entity/collaboration/CollaboratedSession";
import {User} from "../entity/user/User";
import {Collaborator} from "../entity/collaboration/Collaborator";
import NotFound from "../../http/response/exception/NotFound";
import {Session} from "../entity/session/Session";

@EntityRepository(CollaboratedSession)
export class CollaboratedSessionRepository extends Repository<CollaboratedSession> {
    async findByCollaboratedUserId(id: number, isActive: boolean = true) {
        const qb = await getConnection()
            .createQueryBuilder(CollaboratedSession, 'cs')
            .leftJoin('cs.collaborator', 'collaborator')
            .leftJoin('collaborator.user', 'collaboratorUser')
            .where('cs.isActive = :isActive', {isActive: isActive})
            .andWhere('collaboratorUser.id = :userId', {userId: id});

        return qb.getOne();
    }

    async findByCollaboratorAndSessionOrThrow(collaborator: Collaborator, sessionId: number) {
        const qb = await getConnection()
            .createQueryBuilder(CollaboratedSession, 'cs')
            .leftJoinAndSelect('cs.collaborator', 'collaborator')
            .leftJoinAndSelect('collaborator.user', 'collaboratorUser')
            .leftJoinAndSelect('cs.relatedSession', 'session')
            .where('session.id = :sessionId', {sessionId: sessionId})
            .andWhere('cs.isActive = :isActive', {isActive: true})
            .andWhere('collaborator.id = :collaboratorId', {collaboratorId: collaborator.id});

        const collaboratedSession = qb.getOne();
        if (undefined === collaboratedSession) {
            throw new NotFound('Collaborated session was not found');
        }

        return qb.getOne();
    }

    async findByEmail(email: string) {
        const qb = await getConnection()
            .createQueryBuilder(CollaboratedSession, 'cs')
            .leftJoinAndSelect('cs.collaborator', 'collaborator')
            .leftJoinAndSelect('collaborator.user', 'collaboratorUser')
            .leftJoinAndSelect('cs.relatedSession', 'session')
            .where('collaboratorUser.email = :email or collaborator.email = :email', {email: email});

        return qb.getOne();
    }

    async listByUser(user: User) {
        const qb = await getConnection()
            .createQueryBuilder(CollaboratedSession, 'cs')
            .leftJoin('cs.collaborator', 'collaborator')
            .leftJoin('collaborator.user', 'collaboratorUser')
            .leftJoinAndSelect('cs.relatedSession', 'session')
            .leftJoinAndSelect('session.windows', 'windows')
            .leftJoinAndSelect('windows.tabs', 'tabs')
            .where('collaboratorUser.id = :userId', {userId: user.id})
            .andWhere('cs.isConfirmed = :isConfirmed', {isConfirmed: true})
            .andWhere('cs.isActive = :isActive', {isActive: true});

        return qb.getMany();
    }

    async listByOwner(user: User) {
        const qb = await getConnection()
            .createQueryBuilder(CollaboratedSession, 'cs')
            .where('cs.owner = :userId', {userId: user.id});

        return qb.getMany();
    }
}
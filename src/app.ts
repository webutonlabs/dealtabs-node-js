import bodyParser from 'body-parser';
import compression from 'compression'; // compresses requests
import express from 'express';
import {tokenAuthenticator} from "./http/middleware/security/tokenAuthenticator";
import {errorHandler} from "./http/middleware/errorHandler";
import * as Router from "./http/router";
import {resolveRequest} from "./http/middleware/requestResolver";
import {pagination} from 'typeorm-pagination'
import {unless} from "./http/middleware/unless";
import {onlyFor} from "./http/middleware/onlyFor";
import {billingAuthenticator} from "./http/middleware/security/billingAuthenticator";
import {cronAuthenticator} from "./http/middleware/security/cronAuthenticator";
import {corsHeaders} from "./http/middleware/corsHeaders";
import {optionsMethodResponse} from "./http/middleware/optionsMethodResponse";

const app = express();

app.set('port', process.env.NODE_APP_PORT || 3000);
app.set('trust proxy', true);

app.use(compression());
app.use(bodyParser.json());

app.use(corsHeaders);
app.use(optionsMethodResponse);
app.use(resolveRequest);
app.use(onlyFor(['/billing'], billingAuthenticator));
app.use(onlyFor(['/cron'], cronAuthenticator));
app.use(unless(['/auth', '/cron', '/billing'], tokenAuthenticator));
app.use(unless(['/auth', '/cron', '/billing'], pagination));

app.use('/', Router.main);
app.use('/auth', Router.auth);
app.use('/billing', Router.billing);
app.use('/cron', Router.cron);

app.use(errorHandler);

export default app;
import {MembershipTypes} from "../domain/entity/user/Membership";

export interface Membership {
  type: string,
  restrictions: MembershipRestrictions
}

export interface MembershipRestrictions {
  sessionLimit: number | boolean,
  deviceSync: boolean,
  windowsLimit: number | boolean,
  tabsLimit: number | boolean,
  saveSharedSession: boolean,
  quickTabsLimit: number | boolean,
  liveSync: boolean,
  collaboration: boolean
}

export const membershipTypesCollection: Membership[] = [
  {
    type: MembershipTypes.basic,
    restrictions: {
      sessionLimit: 5,
      deviceSync: false,
      windowsLimit: 2,
      tabsLimit: 20,
      saveSharedSession: false,
      quickTabsLimit: 15,
      liveSync: false,
      collaboration: false
    },
  },
  {
    type: MembershipTypes.premium,
    restrictions: {
      sessionLimit: false,
      deviceSync: true,
      windowsLimit: false,
      tabsLimit: false,
      saveSharedSession: true,
      quickTabsLimit: false,
      liveSync: true,
      collaboration: true
    }
  }
];
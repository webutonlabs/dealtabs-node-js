interface GoogleConfig {
    clientId: string,
    clientSecret: string,
    redirectUrl: string
}

const googleConfig: GoogleConfig = {
    clientId: process.env.GOOGLE_APP_ID,
    clientSecret: process.env.GOOGLE_APP_SECRET,
    redirectUrl: process.env.GOOGLE_REDIRECT_URL
}

export {googleConfig}